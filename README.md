DisPar
======

Dispersión Paralelizada (DisPar) es una implementación desarrollada como Proyecto Final de Carrera (PFC) en la  [Universidad de La Laguna][ULL] para el Grupo de Investigación ‘Evolución de galaxias en el Grupo Local’, del [Instituto Astrofísico de Canarias][IAC].

DisPar utiliza la plataforma [Bitbucket Cloud][Bitbucket Cloud] para alojar el repositorio remoto privado, disponible en este [enlace][Remote Repository]. Si se desea obtener privilegios en este repositorio, contactar con los autores. Lo ususarios inexpertos en esta plataforma pueden consultar: [Bitbucket Cloud Documentation][Bitbucket Cloud Documentation].

Detalles
--------

El problema abordado intenta corregir los errores observacionales en la fotometría introducidos por los errores del telescopio, condiciones meteorológicas, etcétera.

Este repositorio ofrece dos implementaciones iterativas que pretenden resolver el problema de forma específica obteniendo un rendimiento mayor. Las implementaciones explotan el paralelismo de los procesadores de un ordenador, a través del uso de las directivas de [OpenMP][OpenMP], y el paralelismo de los nucleos de las tarjetas gráficas NVIDIA con soporte para [CUDA][CUDA]. El lenguaje base del desarrollo es C.

Las implementaciones se localizan en directorios diferentes:
* La implementación iterativa que explota el paralelismo sólo sobre [OpenMP][OpenMP], se encuentra en el directorio `openmp`.
* La implementación iteratica que explota el paralelismo sobre [OpenMP][OpenMP] y [CUDA][CUDA], se encuentra en el directorio `cuda-openmp`.

Requisitos
----------

* El compilador **GCC**.
* [NVIDIA® CUDA® Toolkit 7.5][NVIDIA CUDA Toolkit 7.5].
* [GNU libmatheval][Lib Math Eval].
* [GNU make][GNU make].

Uso
---

Cada implementación requiere un fichero como primer y único parámetro. Dicho fichero contendrá toda la información de los parámetros de ejecución:
* Para la implmentación iterativa [OpenMP][OpenMP] el fichero de parámetros podría ser el siguiente:
```txt
./fichero-madre             # fichero madre entrada
./fichero-crowding          # fichero crowding entrada
./fichero-dispersado        # fichero dispersado salida
./fichero-crowding-ordenado # fichero crowding con anotaciones de uso de estrellas crowding
./fichero-selecciones       # fichero selecciones
4                           # numero de indices 2 o 4
c1                          # expresion madre (columna1) fichero
c1-c2                       # expresion madre (columna1 - columna2 --> color) fichero
c5                          # expresion madre (columna5) fichero
c6                          # expresion madre (columna6) fichero
c1                          # expresion crowding (columna1) fichero
c1-c2                       # expresion crowding (columna1 - columna2 --> color) fichero
c5                          # expresion crowding (columna5) fichero
c6                          # expresion crowding (columna6) fichero
0.1                         # error 1er indice
0.04                        # error 2do indice
300.0                       # error 3er indice
300.0                       # error 4to indice
25.0                        # tolerancia fichero disperso (salida) m1o y m2o
10                          # numero minimo de estrellas a encontrar para dispersion
100000                      # CUDA: numero de estrellas a procesar en cada lote
2                           # CUDA: device donde se lanza la ejecucion del Kernel
0                           # CUDA: numero de threads por bloque
0                           # CUDA: numero de bloques
```
* Para la implmentación iterativa [OpenMP][OpenMP] y [CUDA][CUDA] el fichero de parámetros podría ser el siguiente:
```txt
./fichero-madre             # fichero madre entrada
./fichero-crowding          # fichero crowding entrada
./fichero-dispersado        # fichero dispersado salida
./fichero-crowding-ordenado # fichero crowding con anotaciones de uso de estrellas crowding
./fichero-selecciones       # fichero selecciones
4                           # numero de indices 2 o 4
2                           # tipo de ejecucion 1 --> OPENMP | 2 --> CUDA
c1                          # expresion madre (columna1) fichero
c1-c2                       # expresion madre (columna1 - columna2 --> color) fichero
c5                          # expresion madre (columna5) fichero
c6                          # expresion madre (columna6) fichero
c1                          # expresion crowding (columna1) fichero
c1-c2                       # expresion crowding (columna1 - columna2 --> color) fichero
c5                          # expresion crowding (columna5) fichero
c6                          # expresion crowding (columna6) fichero
0.1                         # error 1er indice
0.04                        # error 2do indice
300.0                       # error 3er indice
300.0                       # error 4to indice
25.0                        # tolerancia fichero disperso (salida) m1o y m2o
10                          # numero minimo de estrellas a encontrar para dispersion
100000                      # CUDA: numero de estrellas a procesar en cada lote
2                           # CUDA: device donde se lanza la ejecucion del Kernel
0                           # CUDA: numero de threads por bloque
0                           # CUDA: numero de bloques
```

Para ejecutar:
* La implmentación iterativa [OpenMP][OpenMP] escribir el comando en el directorio raíz:
```sh
./run_openmp <fichero-parametro>
```
* La implmentación iterativa [OpenMP][OpenMP] y [CUDA][CUDA] escribir el comando en el directorio raíz:
```sh
./run_cuda_openmp <fichero-parametro>
```

Autores
-------

* [Anthony Vittorio Russo Cabrera](https://www.linkedin.com/in/anthonyrussocabrera)
* [Manuel Luis Aznar](https://es.linkedin.com/in/manuel-luis-aznar-6723758a)

[ULL]: <http://www.ull.es/>
[IAC]: <http://iac.es/>
[Bitbucket Cloud]: <https://bitbucket.org/>
[Bitbucket Cloud Documentation]: <https://confluence.atlassian.com/bitbucket>
[Remote Repository]: <https://bitbucket.org/ULLfypIAC/crowding-effects-cli-iterative>
[OpenMP]: <http://openmp.org/>
[CUDA]: <http://www.nvidia.es/object/cuda-parallel-computing-es.html>
[NVIDIA CUDA Toolkit 7.5]: <https://developer.nvidia.com/cuda-toolkit>
[Lib Math Eval]: <https://www.gnu.org/software/libmatheval/>
[GNU make]: <https://www.gnu.org/software/make/manual/make.html>