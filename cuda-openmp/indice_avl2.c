#include "indice_avl2.h"


/* Funciones para la creacion del indice AVL */


IndiceAVL2 CrearIndiceAVL_2(ListaDatosCrowdingExp list, ParametrosEjecucion parametrosEjecucion) {

	IndiceAVL2 indiceA = (pIndiceAVL2) malloc(sizeof(tipoIndiceAVL2));
	indiceA->indice = NULL;
	indiceA->numeroNodosNivel1 = 0;
	indiceA->numeroNodosNivel2 = 0;
	indiceA->numeroEstrellasIndice = 0;

	ID i;
	for (i = 0; i < parametrosEjecucion.totalCrowding; i++) {

		pNodo aux1 = auxCrearIndiceAVL(&indiceA->indice, list[i * parametrosEjecucion.numIndices + EXP_DATO_1], &indiceA->numeroNodosNivel1);

		Arbol arbolNivel2 = (Arbol) aux1->siguiente_nivel;
		pNodo aux2 = auxCrearIndiceAVL(&arbolNivel2, list[i * parametrosEjecucion.numIndices + EXP_DATO_2], &indiceA->numeroNodosNivel2);
		aux1->siguiente_nivel = arbolNivel2;

		ListaNodosEspeciales lista = (pNodoEspecial) aux2->siguiente_nivel;

		pNodoEspecial nuevo = (pNodoEspecial) malloc(sizeof(nodoEspecial));
		nuevo->id = i;

		nuevo->siguiente = lista;
		lista = nuevo;

		aux2->siguiente_nivel = lista;

	}
	indiceA->numeroEstrellasIndice = i;

	return indiceA;
}


/* Funciones para la creacion del indice AVL */


/* Funciones para liberar la memoria del indice AVL */


void auxPodarNivelLista_2(ListaNodosEspeciales *l) {
	pNodoEspecial aux;
	while ((*l) != NULL) {
		aux = (*l);
		(*l) = aux->siguiente;
		free(aux);
	}
}

void auxPodarNivel2_2(Arbol *a) {
	if (*a) {
		auxPodarNivel2_2(&(*a)->izquierdo);
		auxPodarNivel2_2(&(*a)->derecho);
		ListaNodosEspeciales listaNodosEspeciales = (pNodoEspecial) (*a)->siguiente_nivel;
		auxPodarNivelLista_2(&listaNodosEspeciales);
		free(*a);
		*a = NULL;
	}
}

void auxPodarNivel1_2(Arbol *a) {
	if (*a) {
		auxPodarNivel1_2(&(*a)->izquierdo);
		auxPodarNivel1_2(&(*a)->derecho);
		Arbol ptrArbolNivel2 = (Arbol) (*a)->siguiente_nivel;
		auxPodarNivel2_2(&ptrArbolNivel2);
		free(*a);
		*a = NULL;
	}
}

void PodarIndiceAVL_2(IndiceAVL2 *indiceA) {

	auxPodarNivel1_2(&(*indiceA)->indice);

	(*indiceA)->numeroNodosNivel1 = 0;
	(*indiceA)->numeroNodosNivel2 = 0;
	(*indiceA)->numeroEstrellasIndice = 0;

	free(*indiceA);
	*indiceA = NULL;
}


/* Funciones para liberar la memoria del indice AVL */

