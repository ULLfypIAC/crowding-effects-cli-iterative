#ifndef CALCULO_EXPRESIONES_H
#define CALCULO_EXPRESIONES_H

#include <stdlib.h>
#include <stdio.h>
#include <matheval.h>

#include "tipos.h"

void calculoExprCrowding(ListaDatosCrowding listaDatosCrowding, ParametrosEjecucion parametrosEjecucion, ListaDatosCrowdingExp listaDatosCrowdingExp);
void copiarDatosCrowdingParcial(ListaDatosCrowding listaDatosCrowding, ParametrosEjecucion parametrosEjecucion, ListaDatosCrowdingParcial listaDatosCrowdingParcial);
void calculoExprMadre(ListaDatosMadre listaDatosMadre, ParametrosEjecucion parametrosEjecucion, ListaDatosMadreExp listaDatosMadreExp);

#endif