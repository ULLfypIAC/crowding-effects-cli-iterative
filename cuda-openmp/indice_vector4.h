#ifndef INDICE_VECTOR4_H
#define INDICE_VECTOR4_H

#include <stdlib.h>
#include <stdio.h>

#include <cuda.h>
#include <curand.h>
#include <curand_kernel.h>

#include "tipos4.h"
#include "busqueda_binaria.h"


/* Funciones para la creacion del indice Vector */

void auxCrearNivelLista_4(ListaNodosEspeciales lista, IndiceVector4 indiceV);
void auxCrearNivel4_4(Arbol a, IndiceVector4 indiceV);
void auxCrearNivel3_4(Arbol a, IndiceVector4 indiceV);
void auxCrearNivel2_4(Arbol a, IndiceVector4 indiceV);
void auxCrearNivel1_4(Arbol a, IndiceVector4 indiceV);

IndiceVector4 CrearIndiceVector_4(IndiceAVL4 indiceA);

/* Funciones para la creacion del indice Vector */


/* Funcion que libera la memoria del indice Vector */

void PodarIndiceVector_4(IndiceVector4 *indiceV);

/* Funcion que libera la memoria del indice Vector */


/* Funcion de recorrido del indice */

void RecorrerIndiceVector_4(IndiceVector4 indiceV, int *contador, ListaDatosCrowding listCrowd, ListaDatosCrowding list);

/* Funcion de recorrido del indice */


/* Funcion de informacion del indice */

void InformacionIndiceVector_4(IndiceVector4 indiceV, int recorrer);

/* Funcion de informacion del indice */


/* Funcion de busqueda */

__host__ __device__ void BuscarEstrellasIndiceVector_4(IndiceVector4 indiceV, tipoDato *rangos, int *contador, ListaID list, int estrellaDispersoEntero, ListaDatosDispersoEntero *listaDatosDispersoEntero);

/* Funcion de busqueda */

#endif