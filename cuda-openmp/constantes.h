#ifndef CONSTANTES_H
#define CONSTANTES_H

#define TRUE 1
#define FALSE 0

#define EPSILON 1E-10
#define MENOS_EPSILON -EPSILON

#define CONSTANTE_DISPERSION 99.999

#define MAX_TAM_BUFFER 500
#define MAX_LINEAS_FICHERO_PARAMETROS 25


#define MAX_NUM_INDICES 4
#define NUM_ERRORES_2_INDICES 4
#define NUM_ERRORES_4_INDICES 8

#define EXP_DATO_1 0
#define EXP_DATO_2 1
#define EXP_DATO_3 2
#define EXP_DATO_4 3

#define EPSILON_1 0
#define EPSILON_2 1
#define EPSILON_3 2
#define EPSILON_4 3

#define ERROR_DATO_1_MENOS 0
#define ERROR_DATO_1_MAS 1
#define ERROR_DATO_2_MENOS 2
#define ERROR_DATO_2_MAS 3
#define ERROR_DATO_3_MENOS 4
#define ERROR_DATO_3_MAS 5
#define ERROR_DATO_4_MENOS 6
#define ERROR_DATO_4_MAS 7



#define NUMERO_COLUMNAS_MADRE 6
// Columnas que ocupan los datos
// M1, M2, EDAD, METALICIDAD, X e Y en la
// matriz de datos madre (entrada)
#define COL_MADRE_M1 0
#define COL_MADRE_M2 1
#define COL_MADRE_EDAD 2
#define COL_MADRE_METALICIDAD 3
#define COL_MADRE_X 4
#define COL_MADRE_Y 5



#define NUMERO_COLUMNAS_CROWDING 6
// Columnas que ocupan los datos 
// M1I, M2I, M1O, M2O, X e Y en la
// matriz de datos crowding (entrada)
#define COL_CROWDING_M1I 0
#define COL_CROWDING_M2I 1
#define COL_CROWDING_M1O 2
#define COL_CROWDING_M2O 3
#define COL_CROWDING_X   4
#define COL_CROWDING_Y   5



// Numero de columnas de la matriz
// Crowding parcial
#define NUMERO_COLUMNAS_CROWDING_PARCIAL 4
// Columnas que ocupan los datos
// M1I, M2I, M1O y M2O en la
// matriz crowding parcial
#define COL_CROWDING_PARCIAL_M1I 0
#define COL_CROWDING_PARCIAL_M2I 1
#define COL_CROWDING_PARCIAL_M1O 2
#define COL_CROWDING_PARCIAL_M2O 3



// Numero de columnas flotantes
// de la matriz dispersa
#define NUM_COL_DISPERSO_FLOAT 6
// Numero de columnas enteras
// de la matriz dispersa
#define NUM_COL_DISPERSO_INT 3
// Columnas que ocupan los datos
// M1, M2, EDAD, METALICIDAD, X e Y
// en la matriz de datos (flotantes)
// dispersa (salida)
#define COL_DISPERSO_M1 0
#define COL_DISPERSO_M2 1
#define COL_DISPERSO_EDAD 2
#define COL_DISPERSO_METALICIDAD 3
#define COL_DISPERSO_X 4
#define COL_DISPERSO_Y 5
// Columnas que ocupan los datos
// VUELTA, N y OVER_BUFFER
// en la matriz de datos (enteros)
// dispersa (salida)
#define COL_DISPERSO_VUELTA 0
#define COL_DISPERSO_N 1
#define COL_DISPERSO_OVER_BUFFER 2

#endif