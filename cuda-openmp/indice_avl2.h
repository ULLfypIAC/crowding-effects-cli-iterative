#ifndef INDICE_AVL2_H
#define INDICE_AVL2_H

#include "tipos2.h"
#include "avl.h"

/* Funciones para la creacion del indice AVL */

IndiceAVL2 CrearIndiceAVL_2(ListaDatosCrowdingExp list, ParametrosEjecucion parametrosEjecucion);

/* Funciones para la creacion del indice AVL */


/* Funciones para liberar la memoria del indice AVL */

void auxPodarNivelLista_2(ListaNodosEspeciales *l);
void auxPodarNivel2_2(Arbol *a);
void auxPodarNivel1_2(Arbol *a);

void PodarIndiceAVL_2(IndiceAVL2 *indiceA);

/* Funciones para liberar la memoria del indice AVL */

#endif