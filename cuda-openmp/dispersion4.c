#include "dispersion4.h"


/* Funciones de dispersion CUDA y OPENMP (avl) */

__global__ void crowd_avl_4index_4cond_cuda_noUM_O(ListaDatosMadre listaDatosMadre, ListaDatosMadreExp listaDatosMadreExp, IndiceVector4 indiceV, ListaDatosCrowdingParcial listaDatosCrowdingParcial, int tam_total, ParametrosEjecucion parametrosEjecucion, curandState *states, ListaDatosDispersoReal listaDatosDispersoReal, ListaDatosDispersoEntero listaDatosDispersoEntero, ListaUsoEstrellasCrowding listaUsoEstrellasCrowding) {

	int i;
	for (i = blockDim.x * blockIdx.x + threadIdx.x; i < tam_total; i += blockDim.x * gridDim.x) {

		tipoDato rangoMadre[NUM_ERRORES_4_INDICES];

		ID listaEstrellasSeleccionadas[MAX_ESTRELLAS_SELECCIONADAS];

		int contador_estrellas_seleccionadas;
		int k;

		int estrellaMadre;

		int estrellaMadreExp;
		tipoDato estrellaMadreExp_Dato1, estrellaMadreExp_Dato2, estrellaMadreExp_Dato3, estrellaMadreExp_Dato4;

		int estrellaDispersoReal, estrellaDispersoEntero;

		int estrellaCrowdingParcialSelec;

		estrellaMadre = i * NUMERO_COLUMNAS_MADRE;

		estrellaMadreExp = i * parametrosEjecucion.numIndices;
		estrellaMadreExp_Dato1 = listaDatosMadreExp[estrellaMadreExp];
		estrellaMadreExp_Dato2 = listaDatosMadreExp[estrellaMadreExp + EXP_DATO_2];
		estrellaMadreExp_Dato3 = listaDatosMadreExp[estrellaMadreExp + EXP_DATO_3];
		estrellaMadreExp_Dato4 = listaDatosMadreExp[estrellaMadreExp + EXP_DATO_4];

		estrellaDispersoReal = i * NUM_COL_DISPERSO_FLOAT;
		estrellaDispersoEntero = i * NUM_COL_DISPERSO_INT;

		listaDatosDispersoEntero[estrellaDispersoEntero + COL_DISPERSO_OVER_BUFFER] = 0;

		rangoMadre[ERROR_DATO_1_MENOS] = estrellaMadreExp_Dato1 - parametrosEjecucion.epsilons[EPSILON_1];
		rangoMadre[ERROR_DATO_1_MAS] = estrellaMadreExp_Dato1 + parametrosEjecucion.epsilons[EPSILON_1];
		rangoMadre[ERROR_DATO_2_MENOS] = estrellaMadreExp_Dato2 - parametrosEjecucion.epsilons[EPSILON_2];
		rangoMadre[ERROR_DATO_2_MAS] = estrellaMadreExp_Dato2 + parametrosEjecucion.epsilons[EPSILON_2];
		rangoMadre[ERROR_DATO_3_MENOS] = estrellaMadreExp_Dato3 - parametrosEjecucion.epsilons[EPSILON_3];
		rangoMadre[ERROR_DATO_3_MAS] = estrellaMadreExp_Dato3 + parametrosEjecucion.epsilons[EPSILON_3];
		rangoMadre[ERROR_DATO_4_MENOS] = estrellaMadreExp_Dato4 - parametrosEjecucion.epsilons[EPSILON_4];
		rangoMadre[ERROR_DATO_4_MAS] = estrellaMadreExp_Dato4 + parametrosEjecucion.epsilons[EPSILON_4];

		BuscarEstrellasIndiceVector_4(indiceV, rangoMadre, &contador_estrellas_seleccionadas, listaEstrellasSeleccionadas, estrellaDispersoEntero, &listaDatosDispersoEntero);

		k = 1;

		if (contador_estrellas_seleccionadas < parametrosEjecucion.numMinimoEstrellas && listaDatosDispersoEntero[estrellaDispersoEntero + COL_DISPERSO_OVER_BUFFER] == 0) {

			rangoMadre[ERROR_DATO_1_MENOS] -= parametrosEjecucion.epsilons[EPSILON_1];
			rangoMadre[ERROR_DATO_1_MAS] += parametrosEjecucion.epsilons[EPSILON_1];
			rangoMadre[ERROR_DATO_2_MENOS] -= parametrosEjecucion.epsilons[EPSILON_2];
			rangoMadre[ERROR_DATO_2_MAS] += parametrosEjecucion.epsilons[EPSILON_2];
			rangoMadre[ERROR_DATO_3_MENOS] -= parametrosEjecucion.epsilons[EPSILON_3];
			rangoMadre[ERROR_DATO_3_MAS] += parametrosEjecucion.epsilons[EPSILON_3];
			rangoMadre[ERROR_DATO_4_MENOS] -= parametrosEjecucion.epsilons[EPSILON_4];
			rangoMadre[ERROR_DATO_4_MAS] += parametrosEjecucion.epsilons[EPSILON_4];

			BuscarEstrellasIndiceVector_4(indiceV, rangoMadre, &contador_estrellas_seleccionadas, listaEstrellasSeleccionadas, estrellaDispersoEntero, &listaDatosDispersoEntero);

			k = 2;
		}

		if (contador_estrellas_seleccionadas < parametrosEjecucion.numMinimoEstrellas) {
			listaDatosDispersoReal[estrellaDispersoReal] = listaDatosMadre[estrellaMadre];
			listaDatosDispersoReal[estrellaDispersoReal + COL_DISPERSO_M2] = listaDatosMadre[estrellaMadre + COL_MADRE_M2];
			listaDatosDispersoReal[estrellaDispersoReal + COL_DISPERSO_EDAD] = listaDatosMadre[estrellaMadre + COL_MADRE_EDAD];
			listaDatosDispersoReal[estrellaDispersoReal + COL_DISPERSO_METALICIDAD] = listaDatosMadre[estrellaMadre + COL_MADRE_METALICIDAD];
			listaDatosDispersoReal[estrellaDispersoReal + COL_DISPERSO_X] = listaDatosMadre[estrellaMadre + COL_MADRE_X];
			listaDatosDispersoReal[estrellaDispersoReal + COL_DISPERSO_Y] = listaDatosMadre[estrellaMadre + COL_MADRE_Y];
			listaDatosDispersoEntero[estrellaDispersoEntero] = 0;
		}
		else {
			float myrandf = curand_uniform(&(states[i]));
			myrandf *= ((contador_estrellas_seleccionadas - 1) + 0.999999);
			int rand_selec = (int)truncf(myrandf);
			ID id_selec = listaEstrellasSeleccionadas[rand_selec];
			atomicAdd(&listaUsoEstrellasCrowding[id_selec], 1);
			estrellaCrowdingParcialSelec = id_selec * NUMERO_COLUMNAS_CROWDING_PARCIAL;
			tipoDato m1o = listaDatosCrowdingParcial[estrellaCrowdingParcialSelec + COL_CROWDING_PARCIAL_M1O];
			tipoDato m2o = listaDatosCrowdingParcial[estrellaCrowdingParcialSelec + COL_CROWDING_PARCIAL_M2O];
			if ((mayor(m1o, parametrosEjecucion.tolerancia)) || (mayor(m2o, parametrosEjecucion.tolerancia))) {
				listaDatosDispersoReal[estrellaDispersoReal] = CONSTANTE_DISPERSION;
				listaDatosDispersoReal[estrellaDispersoReal + COL_DISPERSO_M2] = CONSTANTE_DISPERSION;
				listaDatosDispersoReal[estrellaDispersoReal + COL_DISPERSO_EDAD] = listaDatosMadre[estrellaMadre + COL_MADRE_EDAD];
				listaDatosDispersoReal[estrellaDispersoReal + COL_DISPERSO_METALICIDAD] = listaDatosMadre[estrellaMadre + COL_MADRE_METALICIDAD];
				listaDatosDispersoReal[estrellaDispersoReal + COL_DISPERSO_X] = listaDatosMadre[estrellaMadre + COL_MADRE_X];
				listaDatosDispersoReal[estrellaDispersoReal + COL_DISPERSO_Y] = listaDatosMadre[estrellaMadre + COL_MADRE_Y];
			}
			else {
				listaDatosDispersoReal[estrellaDispersoReal] =	listaDatosMadre[estrellaMadre] - (listaDatosCrowdingParcial[estrellaCrowdingParcialSelec] - m1o);
				listaDatosDispersoReal[estrellaDispersoReal + COL_DISPERSO_M2] = listaDatosMadre[estrellaMadre + COL_MADRE_M2] - (listaDatosCrowdingParcial[estrellaCrowdingParcialSelec + COL_CROWDING_PARCIAL_M2I] - m2o);
				listaDatosDispersoReal[estrellaDispersoReal + COL_DISPERSO_EDAD] = listaDatosMadre[estrellaMadre + COL_MADRE_EDAD];
				listaDatosDispersoReal[estrellaDispersoReal + COL_DISPERSO_METALICIDAD] = listaDatosMadre[estrellaMadre + COL_MADRE_METALICIDAD];
				listaDatosDispersoReal[estrellaDispersoReal + COL_DISPERSO_X] = listaDatosMadre[estrellaMadre + COL_MADRE_X];
				listaDatosDispersoReal[estrellaDispersoReal + COL_DISPERSO_Y] = listaDatosMadre[estrellaMadre + COL_MADRE_Y];
			}
			listaDatosDispersoEntero[estrellaDispersoEntero] = k;
		}
		listaDatosDispersoEntero[estrellaDispersoEntero + COL_DISPERSO_N] = contador_estrellas_seleccionadas;
	}
}

void crowd_avl_4index_4cond_openmp_O(ListaDatosMadre listaDatosMadre, ListaDatosMadreExp listaDatosMadreExp, IndiceVector4 indiceV, ListaDatosCrowdingParcial listaDatosCrowdingParcial, ParametrosEjecucion parametrosEjecucion, ListaDatosDispersoReal listaDatosDispersoReal, ListaDatosDispersoEntero listaDatosDispersoEntero, ListaUsoEstrellasCrowding listaUsoEstrellasCrowding) {


	// Lista estatica de enteros para guardar los IDs de las estrellas seleccionadas
	ID listaEstrellasSeleccionadas[MAX_ESTRELLAS_SELECCIONADAS];

	int i, k, contador_estrellas_seleccionadas, rand_selec;
	ID id_selec;
	tipoDato rangoMadre[NUM_ERRORES_4_INDICES];

	// tomar la hora del sistema para generacion de semillas
	int horaSistema = (int) time(NULL);

	// variable semilla para generacion de numeros aleatorios
	unsigned int semilla;

	int estrellaMadre;

	int estrellaMadreExp;
	tipoDato estrellaMadreExp_Dato1, estrellaMadreExp_Dato2, estrellaMadreExp_Dato3, estrellaMadreExp_Dato4;

	int estrellaDispersoReal, estrellaDispersoEntero;

	int estrellaCrowdingParcialSelec;

	#pragma omp parallel	private(i, \
					k, \
					contador_estrellas_seleccionadas, \
					rand_selec, \
					id_selec, \
					listaEstrellasSeleccionadas, \
					rangoMadre, \
					estrellaMadre, \
					estrellaMadreExp, \
					estrellaMadreExp_Dato1, \
					estrellaMadreExp_Dato2, \
					estrellaMadreExp_Dato3, \
					estrellaMadreExp_Dato4, \
					estrellaDispersoReal, \
					estrellaDispersoEntero, \
					estrellaCrowdingParcialSelec, \
					semilla) \
				shared(	indiceV, \
					listaDatosCrowdingParcial, \
					listaUsoEstrellasCrowding, \
					listaDatosMadre, \
					parametrosEjecucion, \
					listaDatosMadreExp, \
					listaDatosDispersoReal, \
					listaDatosDispersoEntero, \
					horaSistema)
	{

		semilla = horaSistema ^ omp_get_thread_num();

		#pragma omp for
		for (i = 0; i < parametrosEjecucion.totalMadre; i++) {

			estrellaMadre = i * NUMERO_COLUMNAS_MADRE;

			estrellaMadreExp = i * parametrosEjecucion.numIndices;
			estrellaMadreExp_Dato1 = listaDatosMadreExp[estrellaMadreExp];
			estrellaMadreExp_Dato2 = listaDatosMadreExp[estrellaMadreExp + EXP_DATO_2];
			estrellaMadreExp_Dato3 = listaDatosMadreExp[estrellaMadreExp + EXP_DATO_3];
			estrellaMadreExp_Dato4 = listaDatosMadreExp[estrellaMadreExp + EXP_DATO_4];

			estrellaDispersoReal = i * NUM_COL_DISPERSO_FLOAT;
			estrellaDispersoEntero = i * NUM_COL_DISPERSO_INT;

			listaDatosDispersoEntero[estrellaDispersoEntero + COL_DISPERSO_OVER_BUFFER] = 0;

			rangoMadre[ERROR_DATO_1_MENOS] = estrellaMadreExp_Dato1 - parametrosEjecucion.epsilons[EPSILON_1];
			rangoMadre[ERROR_DATO_1_MAS] = estrellaMadreExp_Dato1 + parametrosEjecucion.epsilons[EPSILON_1];
			rangoMadre[ERROR_DATO_2_MENOS] = estrellaMadreExp_Dato2 - parametrosEjecucion.epsilons[EPSILON_2];
			rangoMadre[ERROR_DATO_2_MAS] = estrellaMadreExp_Dato2 + parametrosEjecucion.epsilons[EPSILON_2];
			rangoMadre[ERROR_DATO_3_MENOS] = estrellaMadreExp_Dato3 - parametrosEjecucion.epsilons[EPSILON_3];
			rangoMadre[ERROR_DATO_3_MAS] = estrellaMadreExp_Dato3 + parametrosEjecucion.epsilons[EPSILON_3];
			rangoMadre[ERROR_DATO_4_MENOS] = estrellaMadreExp_Dato4 - parametrosEjecucion.epsilons[EPSILON_4];
			rangoMadre[ERROR_DATO_4_MAS] = estrellaMadreExp_Dato4 + parametrosEjecucion.epsilons[EPSILON_4];

			BuscarEstrellasIndiceVector_4(indiceV, rangoMadre, &contador_estrellas_seleccionadas, listaEstrellasSeleccionadas, estrellaDispersoEntero, &listaDatosDispersoEntero);

			k = 1;

			if (contador_estrellas_seleccionadas < parametrosEjecucion.numMinimoEstrellas && listaDatosDispersoEntero[estrellaDispersoEntero + COL_DISPERSO_OVER_BUFFER] == 0) {

				rangoMadre[ERROR_DATO_1_MENOS] -= parametrosEjecucion.epsilons[EPSILON_1];
				rangoMadre[ERROR_DATO_1_MAS] += parametrosEjecucion.epsilons[EPSILON_1];
				rangoMadre[ERROR_DATO_2_MENOS] -= parametrosEjecucion.epsilons[EPSILON_2];
				rangoMadre[ERROR_DATO_2_MAS] += parametrosEjecucion.epsilons[EPSILON_2];
				rangoMadre[ERROR_DATO_3_MENOS] -= parametrosEjecucion.epsilons[EPSILON_3];
				rangoMadre[ERROR_DATO_3_MAS] += parametrosEjecucion.epsilons[EPSILON_3];
				rangoMadre[ERROR_DATO_4_MENOS] -= parametrosEjecucion.epsilons[EPSILON_4];
				rangoMadre[ERROR_DATO_4_MAS] += parametrosEjecucion.epsilons[EPSILON_4];

				BuscarEstrellasIndiceVector_4(indiceV, rangoMadre, &contador_estrellas_seleccionadas, listaEstrellasSeleccionadas, estrellaDispersoEntero, &listaDatosDispersoEntero);

				k = 2;
			}

			if (contador_estrellas_seleccionadas < parametrosEjecucion.numMinimoEstrellas) {
				listaDatosDispersoReal[estrellaDispersoReal] = listaDatosMadre[estrellaMadre];
				listaDatosDispersoReal[estrellaDispersoReal + COL_DISPERSO_M2] = listaDatosMadre[estrellaMadre + COL_MADRE_M2];
				listaDatosDispersoReal[estrellaDispersoReal + COL_DISPERSO_EDAD] = listaDatosMadre[estrellaMadre + COL_MADRE_EDAD];
				listaDatosDispersoReal[estrellaDispersoReal + COL_DISPERSO_METALICIDAD] = listaDatosMadre[estrellaMadre + COL_MADRE_METALICIDAD];
				listaDatosDispersoReal[estrellaDispersoReal + COL_DISPERSO_X] = listaDatosMadre[estrellaMadre + COL_MADRE_X];
				listaDatosDispersoReal[estrellaDispersoReal + COL_DISPERSO_Y] = listaDatosMadre[estrellaMadre + COL_MADRE_Y];
				listaDatosDispersoEntero[estrellaDispersoEntero] = 0;
			}
			else {
				rand_selec = rand_r(&semilla) % contador_estrellas_seleccionadas;
				id_selec = listaEstrellasSeleccionadas[rand_selec];
				#pragma omp atomic
				listaUsoEstrellasCrowding[id_selec]++;
				estrellaCrowdingParcialSelec = id_selec * NUMERO_COLUMNAS_CROWDING_PARCIAL;
				tipoDato m1o = listaDatosCrowdingParcial[estrellaCrowdingParcialSelec + COL_CROWDING_PARCIAL_M1O];
				tipoDato m2o = listaDatosCrowdingParcial[estrellaCrowdingParcialSelec + COL_CROWDING_PARCIAL_M2O];
				if ((mayor(m1o, parametrosEjecucion.tolerancia)) || (mayor(m2o, parametrosEjecucion.tolerancia))) {
					listaDatosDispersoReal[estrellaDispersoReal] = CONSTANTE_DISPERSION;
					listaDatosDispersoReal[estrellaDispersoReal + COL_DISPERSO_M2] = CONSTANTE_DISPERSION;
					listaDatosDispersoReal[estrellaDispersoReal + COL_DISPERSO_EDAD] = listaDatosMadre[estrellaMadre + COL_MADRE_EDAD];
					listaDatosDispersoReal[estrellaDispersoReal + COL_DISPERSO_METALICIDAD] = listaDatosMadre[estrellaMadre + COL_MADRE_METALICIDAD];
					listaDatosDispersoReal[estrellaDispersoReal + COL_DISPERSO_X] = listaDatosMadre[estrellaMadre + COL_MADRE_X];
					listaDatosDispersoReal[estrellaDispersoReal + COL_DISPERSO_Y] = listaDatosMadre[estrellaMadre + COL_MADRE_Y];
				}
				else {
					listaDatosDispersoReal[estrellaDispersoReal] =	listaDatosMadre[estrellaMadre] - (listaDatosCrowdingParcial[estrellaCrowdingParcialSelec] - m1o);
					listaDatosDispersoReal[estrellaDispersoReal + COL_DISPERSO_M2] = listaDatosMadre[estrellaMadre + COL_MADRE_M2] - (listaDatosCrowdingParcial[estrellaCrowdingParcialSelec + COL_CROWDING_PARCIAL_M2I] - m2o);
					listaDatosDispersoReal[estrellaDispersoReal + COL_DISPERSO_EDAD] = listaDatosMadre[estrellaMadre + COL_MADRE_EDAD];
					listaDatosDispersoReal[estrellaDispersoReal + COL_DISPERSO_METALICIDAD] = listaDatosMadre[estrellaMadre + COL_MADRE_METALICIDAD];
					listaDatosDispersoReal[estrellaDispersoReal + COL_DISPERSO_X] = listaDatosMadre[estrellaMadre + COL_MADRE_X];
					listaDatosDispersoReal[estrellaDispersoReal + COL_DISPERSO_Y] = listaDatosMadre[estrellaMadre + COL_MADRE_Y];
				}
				listaDatosDispersoEntero[estrellaDispersoEntero] = k;
			}
			listaDatosDispersoEntero[estrellaDispersoEntero + COL_DISPERSO_N] = contador_estrellas_seleccionadas;
		}
	}
}

/* Funciones de dispersion CUDA y OPENMP (avl) */

