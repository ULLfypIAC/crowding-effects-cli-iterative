#include <time.h>

#include "cuda_macros.h"
#include "parametros_entrada.h"
#include "crowding_madre.h"
#include "calculo_expresiones.h"
#include "salida_dispersa.h"

#include "indice_avl2.h"
#include "indice_vector2.h"
#include "dispersion2.h"
#include "salida_crowding2.h"

#include "indice_avl4.h"
#include "indice_vector4.h"
#include "dispersion4.h"
#include "salida_crowding4.h"

int main(int argc, char **argv) {

	ParametrosEjecucion parametrosEjecucion = leerParametrosEntrada(argc, argv);

	erroresParametrosEntrada(parametrosEjecucion, argv[1]);

	printf("Fichero de parametros: %s\n", argv[1]);
	mostrarParametrosEntrada(parametrosEjecucion);


	float elapsedTime_read;
	clock_t start_read = clock();


	ListaDatosCrowding listaDatosCrowding = LeerFicheroCrowding_n(&parametrosEjecucion);

	ListaDatosMadre listaDatosMadre = LeerFicheroMadre_n(&parametrosEjecucion);

	ListaDatosDispersoReal listaDatosDispersoReal = (tipoDato *)malloc(parametrosEjecucion.totalMadre * NUM_COL_DISPERSO_FLOAT * sizeof(tipoDato));
	ListaDatosDispersoEntero listaDatosDispersoEntero = (int *)malloc(parametrosEjecucion.totalMadre * NUM_COL_DISPERSO_INT * sizeof(int));

	ListaUsoEstrellasCrowding listaUsoEstrellasCrowding = (int *)malloc(parametrosEjecucion.totalCrowding * sizeof(int));
	memset(listaUsoEstrellasCrowding, 0, parametrosEjecucion.totalCrowding * sizeof(int));


	clock_t stop_read = clock();
	elapsedTime_read = (float)(stop_read - start_read) / (float) CLOCKS_PER_SEC;
	printf("Tiempo para la lectura de los ficheros de entrada y reserva de memoria: %3.4f segundos\n", elapsedTime_read);



	float elapsedTime_expresions;
	clock_t start_expresions = clock();


	ListaDatosCrowdingExp listaDatosCrowdingExp = (pTipoDato)malloc(parametrosEjecucion.totalCrowding * parametrosEjecucion.numIndices * sizeof(tipoDato));
	calculoExprCrowding(listaDatosCrowding, parametrosEjecucion, listaDatosCrowdingExp);

	ListaDatosMadreExp listaDatosMadreExp = (pTipoDato)malloc(parametrosEjecucion.totalMadre * parametrosEjecucion.numIndices * sizeof(tipoDato));
	calculoExprMadre(listaDatosMadre, parametrosEjecucion, listaDatosMadreExp);

	ListaDatosCrowdingParcial listaDatosCrowdingParcial = (pTipoDato)malloc(parametrosEjecucion.totalCrowding * NUMERO_COLUMNAS_CROWDING_PARCIAL * sizeof(tipoDato));
	copiarDatosCrowdingParcial(listaDatosCrowding, parametrosEjecucion, listaDatosCrowdingParcial);


	clock_t stop_expresions = clock();
	elapsedTime_expresions = (float)(stop_expresions - start_expresions) / (float) CLOCKS_PER_SEC;
	printf("Tiempo para el calculo de expresiones: %3.4f segundos\n", elapsedTime_expresions);



	float elapsedTime_indiceVector2, elapsedTime_indiceVector4;

	// Creacion de Indice Vector
	IndiceAVL2 indiceA2;
	IndiceAVL4 indiceA4;
	IndiceVector2 indiceV2;
	IndiceVector4 indiceV4;

	if (parametrosEjecucion.numIndices == 2) {		// Numero de indices 2

		clock_t start_indiceVector2 = clock();

		// Construimos el indice AVL a partir de la lista de estrellas crowding.
		indiceA2 = CrearIndiceAVL_2(listaDatosCrowdingExp, parametrosEjecucion);
		// Creando el indice Vector a partir del indice AVL
		indiceV2 = CrearIndiceVector_2(indiceA2);
		// Liberamos la memoria reservada para crear el indice AVL.
		PodarIndiceAVL_2(&indiceA2);

		clock_t stop_indiceVector2 = clock();
		elapsedTime_indiceVector2 = (float)(stop_indiceVector2 - start_indiceVector2) / (float) CLOCKS_PER_SEC;
		printf("Tiempo para crear indice AVL 2, crear indice Vector 2 y liberar indice AVL 2: %3.4f segundos\n", elapsedTime_indiceVector2);

	}
	else {							// Numero de indices 4

		clock_t start_indiceVector4 = clock();

		// Construimos el indice AVL a partir de la lista de estrellas crowding.
		indiceA4 = CrearIndiceAVL_4(listaDatosCrowdingExp, parametrosEjecucion);
		// Creando el indice Vector a partir del indice AVL
		indiceV4 = CrearIndiceVector_4(indiceA4);
		// Liberamos la memoria reservada para crear el indice AVL.
		PodarIndiceAVL_4(&indiceA4);

		clock_t stop_indiceVector4 = clock();
		elapsedTime_indiceVector4 = (float)(stop_indiceVector4 - start_indiceVector4) / (float) CLOCKS_PER_SEC;
		printf("Tiempo para crear indice AVL 4, crear indice Vector 4 y liberar indice AVL 4: %3.4f segundos\n", elapsedTime_indiceVector4);

	}


	int tamBufferMadres, device;
	dim3 threadsPerBlock;
	dim3 numBlocks;

	float elapsedTime_iMemGeneric;

	ListaDatosCrowding d_listaDatosMadreBuffer;
	ListaDatosMadreExp d_listaDatosMadreExpBuffer;
	ListaDatosCrowdingParcial d_listaDatosCrowdingParcial;
	ListaDatosDispersoReal d_listaDatosDispersoRealBuffer;
	ListaDatosDispersoEntero d_listaDatosDispersoEnteroBuffer;
	ListaUsoEstrellasCrowding d_listaUsoEstrellasCrowding;
	curandState *d_states;
	ParametrosEjecucion d_parametrosEjecucion;

	float elapsedTime_iMemCreate2, elapsedTime_iMemCreate4;

	IndiceVector2 d_indiceV2 = NULL;
	IndiceVector2 h_indiceV2 = NULL;

	ListaIndicePlano d_indiceNivel1;
	ListaIndicePlano d_indiceNivel2;
	ListaIndicePlano d_indiceNivel3;
	ListaIndicePlano d_indiceNivel4;
	ListaID d_vector_ids;

	IndiceVector4 d_indiceV4 = NULL;
	IndiceVector4 h_indiceV4 = NULL;



	if (parametrosEjecucion.tipoEjecucion == 2) {		// CUDA

		// Configurando ejecucion CUDA
		tamBufferMadres = parametrosEjecucion.tamBufferMadres;
		device = parametrosEjecucion.device;
		// comprobacion de que el device donde se quiere procesar esta disponible en el sistema
		//        en caso de que no este disponible abortar la ejecucion
		int deviceCount;
		cudaGetDeviceCount(&deviceCount);
		if (device >= deviceCount) {
			printf("Error: El device %d no esta disponible en el sistema...\n", device);
			printf("El rango de devices valido en el sistema son [0 - %d] (ambos incluidos)...\n", deviceCount - 1);
			exit(EXIT_FAILURE);
		}

		int threads_per_block = parametrosEjecucion.threads_per_block;
		int num_blocks = parametrosEjecucion.num_blocks;
		if (threads_per_block == 0 || num_blocks == 0) {
			// obtener las propiedades del device escogido para procesar
			cudaDeviceProp devProperties;
			cudaGetDeviceProperties(&devProperties, device);
			// Definiendo el numero de threads por bloque y el numero de bloques (cada thread procesa una unica estrella madre)
			threadsPerBlock.x = devProperties.maxThreadsPerBlock;
			numBlocks.x = ((int) (ceil((double)tamBufferMadres / (double)threadsPerBlock.x)));
		}
		else {
			// Definiendo el numero de threads por bloque y el numero de bloques (cada thread procesa mas de una estrella madre)
			threadsPerBlock.x = threads_per_block;
			numBlocks.x = num_blocks;
		}
		cudaSetDevice(device);

		cudaEvent_t start_iMemGeneric, stop_iMemGeneric;
		cudaEventCreate(&start_iMemGeneric);
		cudaEventCreate(&stop_iMemGeneric);

		cudaEventRecord(start_iMemGeneric, 0);

		CHECK(cudaMalloc((void **)&d_listaDatosMadreBuffer, tamBufferMadres * NUMERO_COLUMNAS_MADRE * sizeof(tipoDato)));
		CHECK(cudaMalloc((void **)&d_listaDatosMadreExpBuffer, tamBufferMadres * parametrosEjecucion.numIndices * sizeof(tipoDato)));

		CHECK(cudaMalloc((void **)&d_listaDatosCrowdingParcial, parametrosEjecucion.totalCrowding * NUMERO_COLUMNAS_CROWDING_PARCIAL * sizeof(tipoDato)));
		cudaMemcpy(d_listaDatosCrowdingParcial, listaDatosCrowdingParcial, parametrosEjecucion.totalCrowding * NUMERO_COLUMNAS_CROWDING_PARCIAL * sizeof(tipoDato), cudaMemcpyHostToDevice);

		CHECK(cudaMalloc((void **)&d_listaUsoEstrellasCrowding, parametrosEjecucion.totalCrowding * sizeof(int)));
		cudaMemset(d_listaUsoEstrellasCrowding, 0, parametrosEjecucion.totalCrowding * sizeof(int));

		CHECK(cudaMalloc(&d_states, tamBufferMadres * sizeof(curandState)));

		CHECK(cudaMalloc((void **)&d_listaDatosDispersoRealBuffer, tamBufferMadres * NUM_COL_DISPERSO_FLOAT * sizeof(tipoDato)));
		CHECK(cudaMalloc((void **)&d_listaDatosDispersoEnteroBuffer, tamBufferMadres * NUM_COL_DISPERSO_INT * sizeof(int)));

		CHECK(cudaMalloc((void **)&d_parametrosEjecucion.epsilons, parametrosEjecucion.numIndices * sizeof(tipoDato)));
		cudaMemcpy(d_parametrosEjecucion.epsilons, parametrosEjecucion.epsilons, parametrosEjecucion.numIndices * sizeof(tipoDato), cudaMemcpyHostToDevice);
		d_parametrosEjecucion.numMinimoEstrellas = parametrosEjecucion.numMinimoEstrellas;
		d_parametrosEjecucion.tolerancia = parametrosEjecucion.tolerancia;
		d_parametrosEjecucion.totalCrowding = parametrosEjecucion.totalCrowding;
		d_parametrosEjecucion.numIndices = parametrosEjecucion.numIndices;

		cudaEventRecord(stop_iMemGeneric, 0);
		cudaEventSynchronize(stop_iMemGeneric);

		cudaEventElapsedTime(&elapsedTime_iMemGeneric, start_iMemGeneric, stop_iMemGeneric);
		elapsedTime_iMemGeneric /= 1000;
		printf("Tiempo para crear y copiar datos genericos: %3.4f segundos\n", elapsedTime_iMemGeneric);

		cudaEventDestroy(start_iMemGeneric);
		cudaEventDestroy(stop_iMemGeneric);


		if (parametrosEjecucion.numIndices == 2) {		// Numero de indices 2

			cudaEvent_t start_iMemCreate2, stop_iMemCreate2;
			cudaEventCreate(&start_iMemCreate2);
			cudaEventCreate(&stop_iMemCreate2);

			cudaEventRecord(start_iMemCreate2, 0);

			// Reservas de memoria para el indice plano en el device
			CHECK(cudaMalloc((void **)&d_indiceV2, sizeof(tipoIndiceVector2)));
			CHECK(cudaMalloc((void **)&d_indiceNivel1, indiceV2->numeroNodosNivel1 * sizeof(tipoIndicePlano)));
			CHECK(cudaMalloc((void **)&d_indiceNivel2, indiceV2->numeroNodosNivel2 * sizeof(tipoIndicePlano)));
			CHECK(cudaMalloc((void **)&d_vector_ids, indiceV2->numeroEstrellasIndice * sizeof(ID)));

			// Copiar los datos host2device (correspondientes al indice plano)
			cudaMemcpy(d_indiceNivel1, indiceV2->indiceNivel1, indiceV2->numeroNodosNivel1 * sizeof(tipoIndicePlano), cudaMemcpyHostToDevice);
			cudaMemcpy(d_indiceNivel2, indiceV2->indiceNivel2, indiceV2->numeroNodosNivel2 * sizeof(tipoIndicePlano), cudaMemcpyHostToDevice);
			cudaMemcpy(d_vector_ids, indiceV2->vector_ids, indiceV2->numeroEstrellasIndice * sizeof(ID), cudaMemcpyHostToDevice);

			// Indice plano en el host que sera rellenado para pasarlo al device
			h_indiceV2 = (pIndiceVector2) malloc(sizeof(tipoIndiceVector2));

			// Rellenando el indice plano con los datos y con direcciones del device
			h_indiceV2->numeroNodosNivel1 = indiceV2->numeroNodosNivel1;
			h_indiceV2->indiceNivel1 = d_indiceNivel1;
			h_indiceV2->numeroNodosNivel2 = indiceV2->numeroNodosNivel2;
			h_indiceV2->indiceNivel2 = d_indiceNivel2;
			h_indiceV2->numeroEstrellasIndice = indiceV2->numeroEstrellasIndice;
			h_indiceV2->vector_ids = d_vector_ids;

			// Copiando el indice plano del host en el indice plano del device 
			cudaMemcpy(d_indiceV2, h_indiceV2, sizeof(tipoIndiceVector2), cudaMemcpyHostToDevice);


			cudaEventRecord(stop_iMemCreate2, 0);
			cudaEventSynchronize(stop_iMemCreate2);

			cudaEventElapsedTime(&elapsedTime_iMemCreate2, start_iMemCreate2, stop_iMemCreate2);
			elapsedTime_iMemCreate2 /= 1000;
			printf("Tiempo para copiar indice Vector 2: %3.4f segundos\n", elapsedTime_iMemCreate2);

			cudaEventDestroy(start_iMemCreate2);
			cudaEventDestroy(stop_iMemCreate2);


		}
		else {							// Numero de indices 4

			cudaEvent_t start_iMemCreate4, stop_iMemCreate4;
			cudaEventCreate(&start_iMemCreate4);
			cudaEventCreate(&stop_iMemCreate4);

			cudaEventRecord(start_iMemCreate4, 0);


			// Reservas de memoria para el indice plano en el device
			CHECK(cudaMalloc((void **)&d_indiceV4, sizeof(tipoIndiceVector4)));
			CHECK(cudaMalloc((void **)&d_indiceNivel1, indiceV4->numeroNodosNivel1 * sizeof(tipoIndicePlano)));
			CHECK(cudaMalloc((void **)&d_indiceNivel2, indiceV4->numeroNodosNivel2 * sizeof(tipoIndicePlano)));
			CHECK(cudaMalloc((void **)&d_indiceNivel3, indiceV4->numeroNodosNivel3 * sizeof(tipoIndicePlano)));
			CHECK(cudaMalloc((void **)&d_indiceNivel4, indiceV4->numeroNodosNivel4 * sizeof(tipoIndicePlano)));
			CHECK(cudaMalloc((void **)&d_vector_ids, indiceV4->numeroEstrellasIndice * sizeof(ID)));

			// Copiar los datos host2device (correspondientes al indice plano)
			cudaMemcpy(d_indiceNivel1, indiceV4->indiceNivel1, indiceV4->numeroNodosNivel1 * sizeof(tipoIndicePlano), cudaMemcpyHostToDevice);
			cudaMemcpy(d_indiceNivel2, indiceV4->indiceNivel2, indiceV4->numeroNodosNivel2 * sizeof(tipoIndicePlano), cudaMemcpyHostToDevice);
			cudaMemcpy(d_indiceNivel3, indiceV4->indiceNivel3, indiceV4->numeroNodosNivel3 * sizeof(tipoIndicePlano), cudaMemcpyHostToDevice);
			cudaMemcpy(d_indiceNivel4, indiceV4->indiceNivel4, indiceV4->numeroNodosNivel4 * sizeof(tipoIndicePlano), cudaMemcpyHostToDevice);
			cudaMemcpy(d_vector_ids, indiceV4->vector_ids, indiceV4->numeroEstrellasIndice * sizeof(ID), cudaMemcpyHostToDevice);

			// Indice plano en el host que sera rellenado para pasarlo al device
			h_indiceV4 = (pIndiceVector4) malloc(sizeof(tipoIndiceVector4));

			// Rellenando el indice plano con los datos y con direcciones del device
			h_indiceV4->numeroNodosNivel1 = indiceV4->numeroNodosNivel1;
			h_indiceV4->indiceNivel1 = d_indiceNivel1;
			h_indiceV4->numeroNodosNivel2 = indiceV4->numeroNodosNivel2;
			h_indiceV4->indiceNivel2 = d_indiceNivel2;
			h_indiceV4->numeroNodosNivel3 = indiceV4->numeroNodosNivel3;
			h_indiceV4->indiceNivel3 = d_indiceNivel3;
			h_indiceV4->numeroNodosNivel4 = indiceV4->numeroNodosNivel4;
			h_indiceV4->indiceNivel4 = d_indiceNivel4;
			h_indiceV4->numeroEstrellasIndice = indiceV4->numeroEstrellasIndice;
			h_indiceV4->vector_ids = d_vector_ids;

			// Copiando el indice plano del host en el indice plano del device 
			cudaMemcpy(d_indiceV4, h_indiceV4, sizeof(tipoIndiceVector4), cudaMemcpyHostToDevice);


			cudaEventRecord(stop_iMemCreate4, 0);
			cudaEventSynchronize(stop_iMemCreate4);

			cudaEventElapsedTime(&elapsedTime_iMemCreate4, start_iMemCreate4, stop_iMemCreate4);
			elapsedTime_iMemCreate4 /= 1000;
			printf("Tiempo para copiar indice Vector 4: %3.4f segundos\n", elapsedTime_iMemCreate4);

			cudaEventDestroy(start_iMemCreate4);
			cudaEventDestroy(stop_iMemCreate4);

		}

	}


	double elapsedTime_alg_avl_openmp_2, elapsedTime_alg_avl_openmp_4;
	float elapsedTime_alg_avl_cuda_2, elapsedTime_alg_avl_cuda_4;

	if (parametrosEjecucion.tipoEjecucion == 1) {							// OPENMP

		if (parametrosEjecucion.numIndices == 2) {								// Numero de indices 2

			double start_alg = omp_get_wtime();

			crowd_avl_2index_2cond_openmp_O(listaDatosMadre, listaDatosMadreExp, indiceV2, listaDatosCrowdingParcial, parametrosEjecucion, listaDatosDispersoReal, listaDatosDispersoEntero, listaUsoEstrellasCrowding);

			double stop_alg = omp_get_wtime();
			elapsedTime_alg_avl_openmp_2 = stop_alg - start_alg;
			printf("Tiempo para algoritmo: %3.4f segundos\n", elapsedTime_alg_avl_openmp_2);

		}
		else {													// Numero de indices 4

			double start_alg = omp_get_wtime();

			crowd_avl_4index_4cond_openmp_O(listaDatosMadre, listaDatosMadreExp, indiceV4, listaDatosCrowdingParcial, parametrosEjecucion, listaDatosDispersoReal, listaDatosDispersoEntero, listaUsoEstrellasCrowding);

			double stop_alg = omp_get_wtime();
			elapsedTime_alg_avl_openmp_4 = stop_alg - start_alg;
			printf("Tiempo para algoritmo: %3.4f segundos\n", elapsedTime_alg_avl_openmp_4);

		}

	} else {											// CUDA

		int i, j;

		// semilla para numeros aleatorios basada en la hora del sistema
		time_t semilla;

		// offsets de los bufferes: DatosMadre, DatosMadreExp, DatosDispersoReal y DatosDispersoEntero
		int offsetBufferDatosMadre, offsetBufferDatosMadreExp, offsetBufferDatosDispersoReal, offsetBufferDatosDispersoEntero;

		if (parametrosEjecucion.numIndices == 2) {

			cudaEvent_t start_alg_avl_2cond, stop_alg_avl_2cond;
			cudaEventCreate(&start_alg_avl_2cond);
			cudaEventCreate(&stop_alg_avl_2cond);

			cudaEventRecord(start_alg_avl_2cond, 0);

			for (i = 0, j = 1; i < parametrosEjecucion.totalMadre; i += tamBufferMadres, j++) {

				// El total de estrellas madres a procesar no es multiplo exacto del tamanio del buffer
				if ((parametrosEjecucion.totalMadre - i) < tamBufferMadres)
					tamBufferMadres = parametrosEjecucion.totalMadre - i;

				semilla = time(NULL);

				// Inicializar un generador de numeros aleatorios para cada madre
				inicializar_estados_prng << <numBlocks, threadsPerBlock >> >(d_states, semilla, tamBufferMadres);
				cudaDeviceSynchronize();

				offsetBufferDatosMadre = i * NUMERO_COLUMNAS_MADRE;
				offsetBufferDatosMadreExp = i * parametrosEjecucion.numIndices;

				// Copiar el lote de estrellas madres host2device
				cudaMemcpy(d_listaDatosMadreBuffer, &listaDatosMadre[offsetBufferDatosMadre], tamBufferMadres * NUMERO_COLUMNAS_MADRE * sizeof(tipoDato), cudaMemcpyHostToDevice);
				cudaMemcpy(d_listaDatosMadreExpBuffer, &listaDatosMadreExp[offsetBufferDatosMadreExp], tamBufferMadres * parametrosEjecucion.numIndices * sizeof(tipoDato), cudaMemcpyHostToDevice);

				// Lanzando la llamada al kernel
				crowd_avl_2index_2cond_cuda_noUM_O << <numBlocks, threadsPerBlock >> >(d_listaDatosMadreBuffer, d_listaDatosMadreExpBuffer, d_indiceV2, d_listaDatosCrowdingParcial, tamBufferMadres, d_parametrosEjecucion, d_states, d_listaDatosDispersoRealBuffer, d_listaDatosDispersoEnteroBuffer, d_listaUsoEstrellasCrowding);

				cudaDeviceSynchronize();

				offsetBufferDatosDispersoReal = i * NUM_COL_DISPERSO_FLOAT;
				offsetBufferDatosDispersoEntero = i * NUM_COL_DISPERSO_INT;

				// Copiar el lote de estrellas dispersas device2hots
				cudaMemcpy(&listaDatosDispersoReal[offsetBufferDatosDispersoReal], d_listaDatosDispersoRealBuffer, tamBufferMadres * NUM_COL_DISPERSO_FLOAT * sizeof(tipoDato), cudaMemcpyDeviceToHost);
				cudaMemcpy(&listaDatosDispersoEntero[offsetBufferDatosDispersoEntero], d_listaDatosDispersoEnteroBuffer, tamBufferMadres * NUM_COL_DISPERSO_INT * sizeof(int), cudaMemcpyDeviceToHost);

				//printf("Lote %d: Procesadas %d estrellas madres\n", j, tamBufferMadres);

			}
			//printf("Procesadas %d estrellas madres\n", parametrosEjecucion.totalMadre);
			cudaMemcpy(listaUsoEstrellasCrowding, d_listaUsoEstrellasCrowding, parametrosEjecucion.totalCrowding * sizeof(int), cudaMemcpyDeviceToHost);

			cudaEventRecord(stop_alg_avl_2cond, 0);
			cudaEventSynchronize(stop_alg_avl_2cond);

			cudaEventElapsedTime(&elapsedTime_alg_avl_cuda_2, start_alg_avl_2cond, stop_alg_avl_2cond);
			elapsedTime_alg_avl_cuda_2 /= 1000;
			printf("Tiempo para algoritmo y copias DeviceToHost: %3.4f segundos\n", elapsedTime_alg_avl_cuda_2);

			cudaEventDestroy(start_alg_avl_2cond);
			cudaEventDestroy(stop_alg_avl_2cond);

		}
		else {

			cudaEvent_t start_alg_avl_4cond, stop_alg_avl_4cond;
			cudaEventCreate(&start_alg_avl_4cond);
			cudaEventCreate(&stop_alg_avl_4cond);

			cudaEventRecord(start_alg_avl_4cond, 0);

			for (i = 0, j = 1; i < parametrosEjecucion.totalMadre; i += tamBufferMadres, j++) {

				// El total de estrellas madres a procesar no es multiplo exacto del tamanio del buffer
				if ((parametrosEjecucion.totalMadre - i) < tamBufferMadres)
					tamBufferMadres = parametrosEjecucion.totalMadre - i;

				semilla = time(NULL);

				// Inicializar un generador de numeros aleatorios para cada madre
				inicializar_estados_prng << <numBlocks, threadsPerBlock >> >(d_states, semilla, tamBufferMadres);
				cudaDeviceSynchronize();

				offsetBufferDatosMadre = i * NUMERO_COLUMNAS_MADRE;
				offsetBufferDatosMadreExp = i * parametrosEjecucion.numIndices;

				// Copiar el lote de estrellas madres host2device
				cudaMemcpy(d_listaDatosMadreBuffer, &listaDatosMadre[offsetBufferDatosMadre], tamBufferMadres * NUMERO_COLUMNAS_MADRE * sizeof(tipoDato), cudaMemcpyHostToDevice);
				cudaMemcpy(d_listaDatosMadreExpBuffer, &listaDatosMadreExp[offsetBufferDatosMadreExp], tamBufferMadres * parametrosEjecucion.numIndices * sizeof(tipoDato), cudaMemcpyHostToDevice);

				crowd_avl_4index_4cond_cuda_noUM_O << <numBlocks, threadsPerBlock >> >(d_listaDatosMadreBuffer, d_listaDatosMadreExpBuffer, d_indiceV4, d_listaDatosCrowdingParcial, tamBufferMadres, d_parametrosEjecucion, d_states, d_listaDatosDispersoRealBuffer, d_listaDatosDispersoEnteroBuffer, d_listaUsoEstrellasCrowding);

				cudaDeviceSynchronize();

				offsetBufferDatosDispersoReal = i * NUM_COL_DISPERSO_FLOAT;
				offsetBufferDatosDispersoEntero = i * NUM_COL_DISPERSO_INT;

				// Copiar el lote de estrellas dispersas device2hots
				cudaMemcpy(&listaDatosDispersoReal[offsetBufferDatosDispersoReal], d_listaDatosDispersoRealBuffer, tamBufferMadres * NUM_COL_DISPERSO_FLOAT * sizeof(tipoDato), cudaMemcpyDeviceToHost);
				cudaMemcpy(&listaDatosDispersoEntero[offsetBufferDatosDispersoEntero], d_listaDatosDispersoEnteroBuffer, tamBufferMadres * NUM_COL_DISPERSO_INT * sizeof(int), cudaMemcpyDeviceToHost);

				//printf("Lote %d: Procesadas %d estrellas madres\n", j, tamBufferMadres);

			}
			//printf("Procesadas %d estrellas madres\n", parametrosEjecucion.totalMadre);
			cudaMemcpy(listaUsoEstrellasCrowding, d_listaUsoEstrellasCrowding, parametrosEjecucion.totalCrowding * sizeof(int), cudaMemcpyDeviceToHost);

			cudaEventRecord(stop_alg_avl_4cond, 0);
			cudaEventSynchronize(stop_alg_avl_4cond);

			cudaEventElapsedTime(&elapsedTime_alg_avl_cuda_4, start_alg_avl_4cond, stop_alg_avl_4cond);
			elapsedTime_alg_avl_cuda_4 /= 1000;
			printf("Tiempo para algoritmo y copias DeviceToHost: %3.4f segundos\n", elapsedTime_alg_avl_cuda_4);

			cudaEventDestroy(start_alg_avl_4cond);
			cudaEventDestroy(stop_alg_avl_4cond);

		}
	}



	float elapsedTime_writeO;
	clock_t start_writeO = clock();

	if (parametrosEjecucion.numIndices == 2) {
		imprimir_fichero_crowding_ordenado_con_anotaciones_indice_vector_2(listaDatosCrowding, indiceV2, listaUsoEstrellasCrowding, parametrosEjecucion);
	}
	else {
		imprimir_fichero_crowding_ordenado_con_anotaciones_indice_vector_4(listaDatosCrowding, indiceV4, listaUsoEstrellasCrowding, parametrosEjecucion);
	}

	imprimir_fichero_disperso(listaDatosDispersoEntero, listaDatosDispersoReal, parametrosEjecucion);
	imprimir_fichero_selecciones(listaDatosDispersoEntero, parametrosEjecucion);


	clock_t stop_writeO = clock();
	elapsedTime_writeO = (float)(stop_writeO - start_writeO) / (float) CLOCKS_PER_SEC;
	printf("Tiempo para escritura de ficheros de salida: %3.4f segundos\n", elapsedTime_writeO);



	float elapsedTime_freeMem;
	clock_t start_freeMem = clock();


	free(parametrosEjecucion.epsilons);
	free(listaDatosCrowding);
	free(listaDatosCrowdingExp);
	free(listaDatosCrowdingParcial);
	free(listaDatosMadre);
	free(listaDatosMadreExp);
	free(listaDatosDispersoReal);
	free(listaDatosDispersoEntero);
	free(listaUsoEstrellasCrowding);
	int i;
	for(i = 0; i < MAX_NUM_INDICES; i++) {
		free(parametrosEjecucion.exps_madre[i]);
		free(parametrosEjecucion.exps_crowding[i]);
	}
	free(parametrosEjecucion.exps_madre);
	free(parametrosEjecucion.exps_crowding);

	if (parametrosEjecucion.numIndices == 2) {
		PodarIndiceVector_2(&indiceV2);
	}
	else {
		PodarIndiceVector_4(&indiceV4);
	}

	if (parametrosEjecucion.tipoEjecucion == 2) {		// CUDA

		cudaFree(d_parametrosEjecucion.epsilons);
		cudaFree(d_listaDatosCrowdingParcial);
		cudaFree(d_listaDatosMadreBuffer);
		cudaFree(d_listaDatosMadreExpBuffer);
		cudaFree(d_listaDatosDispersoRealBuffer);
		cudaFree(d_listaDatosDispersoEnteroBuffer);
		cudaFree(d_listaUsoEstrellasCrowding);
		cudaFree(d_states);

		if (parametrosEjecucion.numIndices == 2) {
			cudaFree(d_indiceV2);
			cudaFree(d_indiceNivel1);
			cudaFree(d_indiceNivel2);
			cudaFree(d_vector_ids);

			free(h_indiceV2);
		}
		else {
			cudaFree(d_indiceV4);
			cudaFree(d_indiceNivel1);
			cudaFree(d_indiceNivel2);
			cudaFree(d_indiceNivel3);
			cudaFree(d_indiceNivel4);
			cudaFree(d_vector_ids);

			free(h_indiceV4);
		}
	}


	clock_t stop_freeMem = clock();
	elapsedTime_freeMem = (float)(stop_freeMem - start_freeMem) / (float) CLOCKS_PER_SEC;
	printf("Tiempo para liberacion de memoria: %3.4f segundos\n", elapsedTime_freeMem);

	printf("Fin de la ejecucion\n");

	printf("\n");

	return 0;
}