#ifndef DISPERSION2_H
#define DISPERSION2_H

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include <omp.h>

#include <cuda.h>
#include <curand.h>
#include <curand_kernel.h>

#include "indice_vector2.h"


__global__ void crowd_avl_2index_2cond_cuda_noUM_O(ListaDatosMadre listaDatosMadre, ListaDatosMadreExp listaDatosMadreExp, IndiceVector2 indiceV, ListaDatosCrowdingParcial listaDatosCrowdingParcial, int tam_total, ParametrosEjecucion parametrosEjecucion, curandState *states, ListaDatosDispersoReal listaDatosDispersoReal, ListaDatosDispersoEntero listaDatosDispersoEntero, ListaUsoEstrellasCrowding listaUsoEstrellasCrowding);

void crowd_avl_2index_2cond_openmp_O(ListaDatosMadre listaDatosMadre, ListaDatosMadreExp listaDatosMadreExp, IndiceVector2 indiceV, ListaDatosCrowdingParcial listaDatosCrowdingParcial, ParametrosEjecucion parametrosEjecucion, ListaDatosDispersoReal listaDatosDispersoReal, ListaDatosDispersoEntero listaDatosDispersoEntero, ListaUsoEstrellasCrowding listaUsoEstrellasCrowding);


#endif

