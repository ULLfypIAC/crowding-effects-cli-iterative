#include "salida_crowding2.h"

void imprimir_fichero_crowding_ordenado_con_anotaciones_indice_vector_2(ListaDatosCrowding listCrowd, IndiceVector2 indiceV2, ListaUsoEstrellasCrowding listaUsoEstrellasCrowding, ParametrosEjecucion parametrosEjecucion) {
	FILE *fcrowding;
	fcrowding = fopen(parametrosEjecucion.fichCrowdingSalida, "w");
	if (fcrowding == NULL) {
		printf("Error: No se pudo crear el fichero crowding de salida con nombre %s...\n", parametrosEjecucion.fichCrowdingSalida);
		exit(EXIT_FAILURE);
	}
	int i, j, k;
	for (i = 0; i < indiceV2->numeroNodosNivel1; i++) {

		for (j = indiceV2->indiceNivel1[i].lower; j <= indiceV2->indiceNivel1[i].upper; j++) {

			for (k = indiceV2->indiceNivel2[j].lower; k <= indiceV2->indiceNivel2[j].upper; k++) {

				fprintf(fcrowding, "%3.3lf\t%3.3lf\t%3.3lf\t%3.3lf\t%3.1lf\t%3.1lf\t%d\n",	listCrowd[indiceV2->vector_ids[k] * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_M1I],
														listCrowd[indiceV2->vector_ids[k] * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_M2I],
														listCrowd[indiceV2->vector_ids[k] * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_M1O],
														listCrowd[indiceV2->vector_ids[k] * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_M2O],
														listCrowd[indiceV2->vector_ids[k] * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_X],
														listCrowd[indiceV2->vector_ids[k] * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_Y],
														listaUsoEstrellasCrowding[indiceV2->vector_ids[k]]);

			}
		}
	}
	fclose(fcrowding);
}