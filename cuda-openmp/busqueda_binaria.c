#include "busqueda_binaria.h"

__host__ __device__ int busquedaBinariaIndiceVectorInferior(ListaIndicePlano lista, int lower, int upper, tipoDato dato) {

	if (menor(lista[upper].dato, dato))
		return -1;

	int centro, inf = lower, sup = upper;

	while (inf <= sup) {
		centro = inf + ((sup - inf) / 2);
		if (igual(lista[centro].dato, dato))
			return centro;				// Rangos cerrados [A, b] devolvemos centro. Rangos abiertos (A, b) devolvemos centro + 1.
		else if (menor(dato, lista[centro].dato))
			sup = centro - 1;
		else
			inf = centro + 1;
	}
	return inf;
}

__host__ __device__ int busquedaBinariaIndiceVectorSuperior(ListaIndicePlano lista, int lower, int upper, tipoDato dato) {

	if (mayor(lista[lower].dato, dato))
		return -1;

	int centro, inf = lower, sup = upper;

	while (inf <= sup) {
		centro = inf + ((sup - inf) / 2);
		if (igual(lista[centro].dato, dato))
			return centro;				// Rangos cerrados [a, B] devolvemos centro. Rangos abiertos (a, B) devolvemos centro - 1.
		else if (menor(dato, lista[centro].dato))
			sup = centro - 1;
		else
			inf = centro + 1;
	}
	return sup;
}