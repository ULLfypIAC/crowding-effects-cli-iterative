#ifndef DISPERSION4_H
#define DISPERSION4_H

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include <omp.h>

#include <cuda.h>
#include <curand.h>
#include <curand_kernel.h>

#include "indice_vector4.h"


__global__ void crowd_avl_4index_4cond_cuda_noUM_O(ListaDatosMadre listaDatosMadre, ListaDatosMadreExp listaDatosMadreExp, IndiceVector4 indiceV, ListaDatosCrowdingParcial listaDatosCrowdingParcial, int tam_total, ParametrosEjecucion parametrosEjecucion, curandState *states, ListaDatosDispersoReal listaDatosDispersoReal, ListaDatosDispersoEntero listaDatosDispersoEntero, ListaUsoEstrellasCrowding listaUsoEstrellasCrowding);

void crowd_avl_4index_4cond_openmp_O(ListaDatosMadre listaDatosMadre, ListaDatosMadreExp listaDatosMadreExp, IndiceVector4 indiceV, ListaDatosCrowdingParcial listaDatosCrowdingParcial, ParametrosEjecucion parametrosEjecucion, ListaDatosDispersoReal listaDatosDispersoReal, ListaDatosDispersoEntero listaDatosDispersoEntero, ListaUsoEstrellasCrowding listaUsoEstrellasCrowding);


#endif