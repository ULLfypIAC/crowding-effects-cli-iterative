#ifndef TIPOS2_H
#define TIPOS2_H

#include "tipos.h"

//+-------------------------------------------------------

typedef struct _indiceAVL2 {

	Arbol indice;

	int numeroNodosNivel1;
	int numeroNodosNivel2;
	int numeroEstrellasIndice;

} tipoIndiceAVL2;

typedef tipoIndiceAVL2* IndiceAVL2;
typedef tipoIndiceAVL2* pIndiceAVL2;

//+-------------------------------------------------------

//x-------------------------------------------------------

typedef struct _indiceVector2 {

	int numeroEstrellasIndice;
	ListaID vector_ids;

	int numeroNodosNivel1;
	ListaIndicePlano indiceNivel1;

	int numeroNodosNivel2;
	ListaIndicePlano indiceNivel2;

} tipoIndiceVector2;

typedef tipoIndiceVector2* IndiceVector2;
typedef tipoIndiceVector2* pIndiceVector2;

//x-------------------------------------------------------

#endif