#ifndef CROWDING_MADRE_H
#define CROWDING_MADRE_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "tipos.h"

ListaDatosCrowding LeerFicheroCrowding_n(ParametrosEjecucion *parametrosEjecucion);
ListaDatosMadre LeerFicheroMadre_n(ParametrosEjecucion *parametrosEjecucion);

void EscribirListaDatosCrowdingFichero(ListaDatosCrowding list, int contador, char *nombre_fich);
void EscribirListaDatosCrowdingConsola(ListaDatosCrowding list, int contador);
void EscribirListaDatosMadreFichero(ListaDatosMadre list, int contador, char *nombre_fich);
void EscribirListaDatosMadreConsola(ListaDatosMadre list, int contador);

#endif