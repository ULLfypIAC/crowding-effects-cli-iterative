#include "indice_avl4.h"


/* Funciones para la creacion del indice AVL */


IndiceAVL4 CrearIndiceAVL_4(ListaDatosCrowdingExp list, ParametrosEjecucion parametrosEjecucion) {

	IndiceAVL4 indiceA = (pIndiceAVL4) malloc(sizeof(tipoIndiceAVL4));
	indiceA->indice = NULL;
	indiceA->numeroNodosNivel1 = 0;
	indiceA->numeroNodosNivel2 = 0;
	indiceA->numeroNodosNivel3 = 0;
	indiceA->numeroNodosNivel4 = 0;
	indiceA->numeroEstrellasIndice = 0;

	ID i;
	for (i = 0; i < parametrosEjecucion.totalCrowding; i++) {

		pNodo aux1 = auxCrearIndiceAVL(&indiceA->indice, list[i * parametrosEjecucion.numIndices + EXP_DATO_1], &indiceA->numeroNodosNivel1);

		Arbol arbolNivel2 = (Arbol) aux1->siguiente_nivel;
		pNodo aux2 = auxCrearIndiceAVL(&arbolNivel2, list[i * parametrosEjecucion.numIndices + EXP_DATO_2], &indiceA->numeroNodosNivel2);
		aux1->siguiente_nivel = arbolNivel2;

		Arbol arbolNivel3 = (Arbol) aux2->siguiente_nivel;
		pNodo aux3 = auxCrearIndiceAVL(&arbolNivel3, list[i * parametrosEjecucion.numIndices + EXP_DATO_3], &indiceA->numeroNodosNivel3);
		aux2->siguiente_nivel = arbolNivel3;

		Arbol arbolNivel4 = (Arbol) aux3->siguiente_nivel;
		pNodo aux4 = auxCrearIndiceAVL(&arbolNivel4, list[i * parametrosEjecucion.numIndices + EXP_DATO_4], &indiceA->numeroNodosNivel4);
		aux3->siguiente_nivel = arbolNivel4;

		ListaNodosEspeciales lista = (pNodoEspecial) aux4->siguiente_nivel;

		pNodoEspecial nuevo = (pNodoEspecial) malloc(sizeof(nodoEspecial));
		nuevo->id = i;

		nuevo->siguiente = lista;
		lista = nuevo;

		aux4->siguiente_nivel = lista;

	}
	indiceA->numeroEstrellasIndice = i;

	return indiceA;
}


/* Funciones para la creacion del indice AVL */


/* Funciones para liberar la memoria del indice AVL */


void auxPodarNivelLista_4(ListaNodosEspeciales *l) {
	pNodoEspecial aux;
	while ((*l) != NULL) {
		aux = (*l);
		(*l) = aux->siguiente;
		free(aux);
	}
}

void auxPodarNivel4_4(Arbol *a) {
	if (*a) {
		auxPodarNivel4_4(&(*a)->izquierdo);
		auxPodarNivel4_4(&(*a)->derecho);
		ListaNodosEspeciales listaNodosEspeciales = (pNodoEspecial) (*a)->siguiente_nivel;
		auxPodarNivelLista_4(&listaNodosEspeciales);
		free(*a);
		*a = NULL;
	}
}

void auxPodarNivel3_4(Arbol *a) {
	if (*a) {
		auxPodarNivel3_4(&(*a)->izquierdo);
		auxPodarNivel3_4(&(*a)->derecho);
		Arbol ptrArbolNivel4 = (Arbol) (*a)->siguiente_nivel;
		auxPodarNivel4_4(&ptrArbolNivel4);
		free(*a);
		*a = NULL;
	}
}

void auxPodarNivel2_4(Arbol *a) {
	if (*a) {
		auxPodarNivel2_4(&(*a)->izquierdo);
		auxPodarNivel2_4(&(*a)->derecho);
		Arbol ptrArbolNivel3 = (Arbol) (*a)->siguiente_nivel;
		auxPodarNivel3_4(&ptrArbolNivel3);
		free(*a);
		*a = NULL;
	}
}

void auxPodarNivel1_4(Arbol *a) {
	if (*a) {
		auxPodarNivel1_4(&(*a)->izquierdo);
		auxPodarNivel1_4(&(*a)->derecho);
		Arbol ptrArbolNivel2 = (Arbol) (*a)->siguiente_nivel;
		auxPodarNivel2_4(&ptrArbolNivel2);
		free(*a);
		*a = NULL;
	}
}

void PodarIndiceAVL_4(IndiceAVL4 *indiceA) {

	auxPodarNivel1_4(&(*indiceA)->indice);

	(*indiceA)->numeroNodosNivel1 = 0;
	(*indiceA)->numeroNodosNivel2 = 0;
	(*indiceA)->numeroNodosNivel3 = 0;
	(*indiceA)->numeroNodosNivel4 = 0;
	(*indiceA)->numeroEstrellasIndice = 0;

	free(*indiceA);
	*indiceA = NULL;
}


/* Funciones para liberar la memoria del indice AVL */

