#ifndef TIPOS_H
#define TIPOS_H

#include "constantes.h"

enum {IZQUIERDO, DERECHO};

/* Definicion de tipos de datos y estructuras necesarias */


// Tipo de dato para las entrada y salidas
typedef double tipoDato;

// Tipos de datos para Identificacion de estrellas.
	// ID			--> Indice AVL
	// ID, pID y Lista ID	--> Indice Vector
typedef int ID;
typedef ID* pID;
typedef ID* ListaID;

typedef tipoDato* pTipoDato;

typedef tipoDato* ListaDatosCrowding;
typedef tipoDato* ListaDatosMadre;

typedef tipoDato* ListaDatosCrowdingExp;
typedef tipoDato* ListaDatosMadreExp;

typedef tipoDato* ListaDatosCrowdingParcial;

typedef int* ListaUsoEstrellasCrowding;

typedef int* ListaDatosDispersoEntero;
typedef tipoDato* ListaDatosDispersoReal;

//--------------------------------------------------------

// Estructura para los datos leidos del fichero de parametros
typedef struct REGparametrosEjecucion {
	char fichMadre[MAX_TAM_BUFFER], fichCrowdingEntrada[MAX_TAM_BUFFER], fichDisperso[MAX_TAM_BUFFER], fichCrowdingSalida[MAX_TAM_BUFFER], fichSelecciones[MAX_TAM_BUFFER];
	int numIndices, tipoEjecucion;
	char **exps_madre, **exps_crowding;
	tipoDato *epsilons;
	tipoDato tolerancia;
	int numMinimoEstrellas;
	int tamBufferMadres, device, threads_per_block, num_blocks;
	int totalMadre, totalCrowding;

	int codSalida;
} ParametrosEjecucion;

//--------------------------------------------------------

//+-------------------------------------------------------

typedef struct _nodo_especial {
	ID id;
	struct _nodo_especial *siguiente;
} nodoEspecial;

typedef nodoEspecial* pNodoEspecial;
typedef nodoEspecial* ListaNodosEspeciales;

//+-------------------------------------------------------

typedef struct _tipo_nodo {
	tipoDato dato;
	int FE;

	struct _tipo_nodo *derecho;
	struct _tipo_nodo *izquierdo;
	struct _tipo_nodo *padre;

	void *siguiente_nivel;
} tipoNodo;

typedef tipoNodo* pNodo;
typedef tipoNodo* Arbol;

//+-------------------------------------------------------

//x-------------------------------------------------------

typedef struct _indice_plano {
	tipoDato dato;
	int lower, upper;
} tipoIndicePlano;

typedef tipoIndicePlano* pIndicePlano;
typedef tipoIndicePlano* ListaIndicePlano;

//x-------------------------------------------------------

#endif