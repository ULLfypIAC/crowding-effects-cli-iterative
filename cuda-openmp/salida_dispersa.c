#include "salida_dispersa.h"

void imprimir_fichero_disperso(ListaDatosDispersoEntero listaDatosDispersoEntero, ListaDatosDispersoReal listaDatosDispersoReal, ParametrosEjecucion parametrosEjecucion) {
	FILE *fdisperso;
	fdisperso = fopen(parametrosEjecucion.fichDisperso, "w");
	if (fdisperso == NULL) {
		printf("Error: No se pudo crear el fichero disperso con nombre %s...\n", parametrosEjecucion.fichDisperso);
		exit(EXIT_FAILURE);
	}
	int i;
	for (i = 0; i < parametrosEjecucion.totalMadre; i++)
		fprintf(fdisperso, "%3.3lf\t%3.3lf\t%6.4e\t%6.4e\t%3.1lf\t%3.1lf\t%d\t%d\t%d\n",	listaDatosDispersoReal[i * NUM_COL_DISPERSO_FLOAT + COL_DISPERSO_M1],
													listaDatosDispersoReal[i * NUM_COL_DISPERSO_FLOAT + COL_DISPERSO_M2],
													listaDatosDispersoReal[i * NUM_COL_DISPERSO_FLOAT + COL_DISPERSO_EDAD],
													listaDatosDispersoReal[i * NUM_COL_DISPERSO_FLOAT + COL_DISPERSO_METALICIDAD],
													listaDatosDispersoReal[i * NUM_COL_DISPERSO_FLOAT + COL_DISPERSO_X],
													listaDatosDispersoReal[i * NUM_COL_DISPERSO_FLOAT + COL_DISPERSO_Y],
													listaDatosDispersoEntero[i * NUM_COL_DISPERSO_INT + COL_DISPERSO_VUELTA],
													listaDatosDispersoEntero[i * NUM_COL_DISPERSO_INT + COL_DISPERSO_N],
													listaDatosDispersoEntero[i * NUM_COL_DISPERSO_INT + COL_DISPERSO_OVER_BUFFER]);
	fclose(fdisperso);
}

void imprimir_fichero_selecciones(ListaDatosDispersoEntero listaDatosDispersoEntero, ParametrosEjecucion parametrosEjecucion) {
	FILE *fselecciones;
	fselecciones = fopen(parametrosEjecucion.fichSelecciones, "w");
	if (fselecciones == NULL) {
		printf("Error: No se pudo crear el fichero de selecciones con nombre %s...\n", parametrosEjecucion.fichSelecciones);
		exit(EXIT_FAILURE);
	}
	int i;
	for (i = 0; i < parametrosEjecucion.totalMadre; i++)
		fprintf(fselecciones, "%d\t%d\t%d\n",	i,
							listaDatosDispersoEntero[i * NUM_COL_DISPERSO_INT + COL_DISPERSO_VUELTA],
							listaDatosDispersoEntero[i * NUM_COL_DISPERSO_INT + COL_DISPERSO_N]);
	fclose(fselecciones);
}