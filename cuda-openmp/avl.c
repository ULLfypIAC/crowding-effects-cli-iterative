#include "avl.h"

// Funcion de insercion en un arbol avl
pNodo auxCrearIndiceAVL(Arbol *a, tipoDato dat, int *contador) {

	pNodo padre = NULL;
	pNodo actual = *a;

	/* Buscar el dato en el arbol, manteniendo un puntero al nodo padre */
	while (!Vacio(actual) && !igual(dat, actual->dato)) {
		padre = actual;
		if (menor(dat, actual->dato))
			actual = actual->izquierdo;
		else if (mayor(dat, actual->dato))
			actual = actual->derecho;
	}

	/* Si se ha encontrado el elemento, regresar sin insertar */
	if (!Vacio(actual))
		return actual;
	/* Si padre es NULL, entonces el arbol estaba vacio, el nuevo nodo sera el nodo raiz */
	else if (Vacio(padre)) {
		*a = (Arbol) malloc(sizeof(tipoNodo));
		(*a)->dato = dat;
		(*a)->izquierdo = (*a)->derecho = NULL;
		(*a)->padre = NULL;
		(*a)->FE = 0;
		(*a)->siguiente_nivel = NULL;
		(*contador)++;
		return *a;
	}
	/* Si el dato es menor que el que contiene el nodo padre, lo insertamos en la rama izquierda */
	else if (menor(dat, padre->dato)) {
		actual = (Arbol) malloc(sizeof(tipoNodo));
		padre->izquierdo = actual;
		actual->dato = dat;
		actual->izquierdo = actual->derecho = NULL;
		actual->padre = padre;
		actual->FE = 0;
		actual->siguiente_nivel = NULL;
		Equilibrar(a, padre, IZQUIERDO, TRUE);
		(*contador)++;
		return actual;
	}
	/* Si el dato es mayor que el que contiene el nodo padre, lo insertamos en la rama derecha */
	else {  /*if (mayor(dat, padre->dato)) */
		actual = (Arbol) malloc(sizeof(tipoNodo));
		padre->derecho = actual;
		actual->dato = dat;
		actual->izquierdo = actual->derecho = NULL;
		actual->padre = padre;
		actual->FE = 0;
		actual->siguiente_nivel = NULL;
		Equilibrar(a, padre, DERECHO, TRUE);
		(*contador)++;
		return actual;
	}
}

/* Equilibrar árbol AVL partiendo del nodo nuevo */
void Equilibrar(Arbol *a, pNodo nodo, int rama, int nuevo) {

	int salir = FALSE;

	/* Recorrer camino inverso actualizando valores de FE: */
	while (nodo && !salir) {
		if (nuevo)
			if (rama == IZQUIERDO)
				nodo->FE--; /* Depende de si añadimos ... */
			else
				nodo->FE++;
		else
			if (rama == IZQUIERDO)
				nodo->FE++; /* ... o borramos */
			else
				nodo->FE--;

		if (nodo->FE == 0)
			salir = TRUE; /* La altura de las rama que
					 empieza en nodo no ha variado,
					 salir de Equilibrar */
		else if (nodo->FE == -2) { /* Rotar a derechas y salir: */
			if (nodo->izquierdo->FE == 1)
				RDD(a, nodo); /* Rotación doble  */
			else
				RSD(a, nodo);                         /* Rotación simple */
			salir = TRUE;
		}
		else if (nodo->FE == 2) {  /* Rotar a izquierdas y salir: */
			if (nodo->derecho->FE == -1)
				RDI(a, nodo); /* Rotación doble  */
			else
				RSI(a, nodo);                        /* Rotación simple */
			salir = TRUE;
		}

		if (nodo->padre)
			if (nodo->padre->derecho == nodo)
				rama = DERECHO;
			else
				rama = IZQUIERDO;

		nodo = nodo->padre; /* Calcular FE, siguiente nodo del camino. */
	}

}

/* Rotación doble a derechas */
void RDD(Arbol *raiz, Arbol nodo) {

	pNodo Padre = nodo->padre;
	pNodo P = nodo;
	pNodo Q = P->izquierdo;
	pNodo R = Q->derecho;
	pNodo B = R->izquierdo;
	pNodo C = R->derecho;

	if (Padre)
		if (Padre->derecho == nodo)
			Padre->derecho = R;
		else
			Padre->izquierdo = R;
	else
		*raiz = R;

	/* Reconstruir árbol: */
	Q->derecho = B;
	P->izquierdo = C;
	R->izquierdo = Q;
	R->derecho = P;

	/* Reasignar padres: */
	R->padre = Padre;
	P->padre = Q->padre = R;
	if (B)
		B->padre = Q;
	if (C)
		C->padre = P;

	/* Ajustar valores de FE: */
	switch (R->FE) {
		case -1:
			Q->FE = 0;
			P->FE = 1;
			break;
		case 0:
			Q->FE = 0;
			P->FE = 0;
			break;
		case 1:
			Q->FE = -1;
			P->FE = 0;
			break;
	}

	R->FE = 0;
}

/* Rotación doble a izquierdas */
void RDI(Arbol *a, pNodo nodo) {

	pNodo Padre = nodo->padre;
	pNodo P = nodo;
	pNodo Q = P->derecho;
	pNodo R = Q->izquierdo;
	pNodo B = R->izquierdo;
	pNodo C = R->derecho;

	if (Padre)
		if (Padre->derecho == nodo)
			Padre->derecho = R;
		else
			Padre->izquierdo = R;
	else
		*a = R;

	/* Reconstruir árbol: */
	P->derecho = B;
	Q->izquierdo = C;
	R->izquierdo = P;
	R->derecho = Q;

	/* Reasignar padres: */
	R->padre = Padre;
	P->padre = Q->padre = R;
	if (B)
		B->padre = P;
	if (C)
		C->padre = Q;

	/* Ajustar valores de FE: */
	switch (R->FE) {
		case -1:
			P->FE = 0;
			Q->FE = 1;
			break;
		case 0:
			P->FE = 0;
			Q->FE = 0;
			break;
		case 1:
			P->FE = -1;
			Q->FE = 0;
			break;
	}

	R->FE = 0;

}

/* Rotación simple a derechas */
void RSD(Arbol *a, pNodo nodo) {

	pNodo Padre = nodo->padre;
	pNodo P = nodo;
	pNodo Q = P->izquierdo;
	pNodo B = Q->derecho;

	if (Padre)
		if (Padre->derecho == P)
			Padre->derecho = Q;
		else
			Padre->izquierdo = Q;
	else
		*a = Q;

	/* Reconstruir árbol: */
	P->izquierdo = B;
	Q->derecho = P;

	/* Reasignar padres: */
	P->padre = Q;
	if (B)
		B->padre = P;
	Q->padre = Padre;

	/* Ajustar valores de FE: */
	P->FE = 0;
	Q->FE = 0;

}

/* Rotación simple a izquierdas */
void RSI(Arbol *a, pNodo nodo) {

	pNodo Padre = nodo->padre;
	pNodo P = nodo;
	pNodo Q = P->derecho;
	pNodo B = Q->izquierdo;

	if (Padre)
		if (Padre->derecho == P)
			Padre->derecho = Q;
		else
			Padre->izquierdo = Q;
	else
		*a = Q;

	/* Reconstruir árbol: */
	P->derecho = B;
	Q->izquierdo = P;

	/* Reasignar padres: */
	P->padre = Q;
	if (B)
		B->padre = P;
	Q->padre = Padre;

	/* Ajustar valores de FE: */
	P->FE = 0;
	Q->FE = 0;

}

/* Comprobar si un árbol es vacío */
int Vacio(Arbol r) {
	return r == NULL;
}

