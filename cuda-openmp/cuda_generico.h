#ifndef CUDA_GENERICO_H
#define CUDA_GENERICO_H

#include <stdlib.h>
#include <stdio.h>

#include <cuda.h>
#include <curand.h>
#include <curand_kernel.h>

/* Funciones CUDA */

void _check(cudaError_t r, int line);

__global__ void inicializar_estados_prng(curandState *states, time_t semilla, int tam_total);

/* Funciones CUDA */

#endif