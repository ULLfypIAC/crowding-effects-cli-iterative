#ifndef SALIDA_CROWDING4_H
#define SALIDA_CROWDING4_H

#include <stdlib.h>
#include <stdio.h>

#include "tipos4.h"

void imprimir_fichero_crowding_ordenado_con_anotaciones_indice_vector_4(ListaDatosCrowding listCrowd, IndiceVector4 indiceV4, ListaUsoEstrellasCrowding listaUsoEstrellasCrowding, ParametrosEjecucion parametrosEjecucion);

#endif