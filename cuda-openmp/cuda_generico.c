#include "cuda_generico.h"


/* Funciones CUDA */

void _check(cudaError_t r, int line) {
	if (r != cudaSuccess) {
		printf("CUDA error on line %d: %s\n", line, cudaGetErrorString(r));
		exit(0);
	}
}

/*
    Kernel para inicializar una variable que contiene un generador de numeros aleatorios para cada thread.

    La ejecucion de este Kernel precede al kernel crowding_nm(...).
*/
__global__ void inicializar_estados_prng(curandState *states, time_t semilla, int tam_total) {
	int i;
	for (i = blockDim.x * blockIdx.x + threadIdx.x; i < tam_total; i += blockDim.x * gridDim.x)
		curand_init(semilla + i, i, 0, &states[i]);
}

/* Funciones CUDA */