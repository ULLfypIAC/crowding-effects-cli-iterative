#ifndef PARAMETROS_ENTRADA_H
#define PARAMETROS_ENTRADA_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "tipos.h"

ParametrosEjecucion leerParametrosEntrada(int nparam, char *argv[]);

void mostrarParametrosEntrada(ParametrosEjecucion parametrosEjecucion);

void erroresParametrosEntrada(ParametrosEjecucion parametrosEjecucion, char *nombre_fichero);

#endif
