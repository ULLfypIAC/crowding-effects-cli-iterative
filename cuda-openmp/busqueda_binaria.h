#ifndef BUSQUEDA_BINARIA_H
#define BUSQUEDA_BINARIA_H

#include <cuda.h>
#include <curand.h>
#include <curand_kernel.h>

#include "tipos.h"
#include "macros.h"

__host__ __device__ int busquedaBinariaIndiceVectorInferior(ListaIndicePlano lista, int lower, int upper, tipoDato dato);
__host__ __device__ int busquedaBinariaIndiceVectorSuperior(ListaIndicePlano lista, int lower, int upper, tipoDato dato);

#endif