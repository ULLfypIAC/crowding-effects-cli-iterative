#ifndef INDICE_AVL4_H
#define INDICE_AVL4_H

#include "tipos4.h"
#include "avl.h"


/* Funciones para la creacion del indice AVL */

IndiceAVL4 CrearIndiceAVL_4(ListaDatosCrowdingExp list, ParametrosEjecucion parametrosEjecucion);

/* Funciones para la creacion del indice AVL */


/* Funciones para liberar la memoria del indice AVL */

void auxPodarNivelLista_4(ListaNodosEspeciales *l);
void auxPodarNivel4_4(Arbol *a);
void auxPodarNivel3_4(Arbol *a);
void auxPodarNivel2_4(Arbol *a);
void auxPodarNivel1_4(Arbol *a);

void PodarIndiceAVL_4(IndiceAVL4 *indiceA);

/* Funciones para liberar la memoria del indice AVL */


#endif