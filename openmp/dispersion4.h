#ifndef DISPERSION4_H
#define DISPERSION4_H

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include <omp.h>

#include "indice_vector4.h"

void crowd_avl_4index_4cond_openmp_O(ListaDatosMadre listaDatosMadre, ListaDatosMadreExp listaDatosMadreExp, IndiceVector4 indiceV, ListaDatosCrowdingParcial listaDatosCrowdingParcial, ParametrosEjecucion parametrosEjecucion, ListaDatosDispersoReal listaDatosDispersoReal, ListaDatosDispersoEntero listaDatosDispersoEntero, ListaUsoEstrellasCrowding listaUsoEstrellasCrowding);

#endif