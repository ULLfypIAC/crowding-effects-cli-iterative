#ifndef AVL_H
#define AVL_H

#include <stdlib.h>
#include <stdio.h>

#include "tipos.h"
#include "macros.h"

pNodo auxCrearIndiceAVL(Arbol *a, tipoDato dat, int *contador);

void Equilibrar(Arbol *a, pNodo nodo, int rama, int nuevo);
void RSI(Arbol *raiz, pNodo nodo);
void RSD(Arbol *raiz, pNodo nodo);
void RDI(Arbol *raiz, pNodo nodo);
void RDD(Arbol *raiz, pNodo nodo);
int Vacio(Arbol r);

#endif