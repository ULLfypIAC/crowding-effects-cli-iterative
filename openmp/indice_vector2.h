#ifndef INDICE_VECTOR2_H
#define INDICE_VECTOR2_H

#include <stdlib.h>
#include <stdio.h>

#include "tipos2.h"
#include "busqueda_binaria.h"


/* Funciones para la creacion del indice Vector */

void auxCrearNivelLista_2(ListaNodosEspeciales lista, IndiceVector2 indiceV);
void auxCrearNivel2_2(Arbol a, IndiceVector2 indiceV);
void auxCrearNivel1_2(Arbol a, IndiceVector2 indiceV);

IndiceVector2 CrearIndiceVector_2(IndiceAVL2 indiceA);

/* Funciones para la creacion del indice Vector */


/* Funcion que libera la memoria del indice Vector */

void PodarIndiceVector_2(IndiceVector2 *indiceV);

/* Funcion que libera la memoria del indice Vector */


/* Funcion de recorrido del indice */

void RecorrerIndiceVector_2(IndiceVector2 indiceV, int *contador, ListaDatosCrowding listCrowd, ListaDatosCrowding list);

/* Funcion de recorrido del indice */


/* Funcion de informacion del indice */

void InformacionIndiceVector_2(IndiceVector2 indiceV, int recorrer);

/* Funcion de informacion del indice */


/* Funcion de busqueda */

void BuscarEstrellasIndiceVector_2(IndiceVector2 indiceV, tipoDato *rangos, int *contador, ListaID list, int estrellaDispersoEntero, ListaDatosDispersoEntero *listaDatosDispersoEntero);

/* Funcion de busqueda */

#endif