#include "salida_crowding4.h"

void imprimir_fichero_crowding_ordenado_con_anotaciones_indice_vector_4(ListaDatosCrowding listCrowd, IndiceVector4 indiceV4, ListaUsoEstrellasCrowding listaUsoEstrellasCrowding, ParametrosEjecucion parametrosEjecucion) {
	FILE *fcrowding;
	fcrowding = fopen(parametrosEjecucion.fichCrowdingSalida, "w");
	if (fcrowding == NULL) {
		printf("Error: No se pudo crear el fichero crowding de salida con nombre %s...\n", parametrosEjecucion.fichCrowdingSalida);
		exit(EXIT_FAILURE);
	}
	int i, j, k, l, m;
	for (i = 0; i < indiceV4->numeroNodosNivel1; i++) {

		for (j = indiceV4->indiceNivel1[i].lower; j <= indiceV4->indiceNivel1[i].upper; j++) {

			for (k = indiceV4->indiceNivel2[j].lower; k <= indiceV4->indiceNivel2[j].upper; k++) {

				for (l = indiceV4->indiceNivel3[k].lower; l <= indiceV4->indiceNivel3[k].upper; l++) {

					for (m = indiceV4->indiceNivel4[l].lower; m <= indiceV4->indiceNivel4[l].upper; m++) {

						fprintf(fcrowding, "%3.3lf\t%3.3lf\t%3.3lf\t%3.3lf\t%3.1lf\t%3.1lf\t%d\n",	listCrowd[indiceV4->vector_ids[m] * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_M1I],
																listCrowd[indiceV4->vector_ids[m] * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_M2I],
																listCrowd[indiceV4->vector_ids[m] * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_M1O],
																listCrowd[indiceV4->vector_ids[m] * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_M2O],
																listCrowd[indiceV4->vector_ids[m] * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_X],
																listCrowd[indiceV4->vector_ids[m] * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_Y],
																listaUsoEstrellasCrowding[indiceV4->vector_ids[m]]);

					}
				}
			}
		}
	}
	fclose(fcrowding);
}