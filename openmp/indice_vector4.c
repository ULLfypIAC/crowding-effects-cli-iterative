#include "indice_vector4.h"


/* Funciones para la creacion del indice Vector */

void auxCrearNivelLista_4(ListaNodosEspeciales lista, IndiceVector4 indiceV) {
	pNodoEspecial aux;
	aux = lista;
	while (aux != NULL) {
		indiceV->vector_ids[indiceV->numeroEstrellasIndice] = aux->id;
		indiceV->numeroEstrellasIndice++;
		aux = aux->siguiente;
	}
}

void auxCrearNivel4_4(Arbol a, IndiceVector4 indiceV) {
	if (a->izquierdo)
		auxCrearNivel4_4(a->izquierdo, indiceV);

	indiceV->indiceNivel4[indiceV->numeroNodosNivel4].dato = a->dato;					// guardar el dato del nivel 4 en el indice4

	indiceV->indiceNivel4[indiceV->numeroNodosNivel4].lower = indiceV->numeroEstrellasIndice;		// guardar lower indice4 --> contador vectorIds

	ListaNodosEspeciales lista = (pNodoEspecial) a->siguiente_nivel;					// obtener la lista de IDs
	auxCrearNivelLista_4(lista, indiceV);									// recorrer la lista de IDs

	indiceV->indiceNivel4[indiceV->numeroNodosNivel4].upper = indiceV->numeroEstrellasIndice - 1;		// guardar upper indice4 ---> contador vectorIds - 1

	indiceV->numeroNodosNivel4++;										// avanzar el contador de datos del nivel4

	if(a->derecho)
		auxCrearNivel4_4(a->derecho, indiceV);
}

void auxCrearNivel3_4(Arbol a, IndiceVector4 indiceV) {
	if (a->izquierdo)
		auxCrearNivel3_4(a->izquierdo, indiceV);

	indiceV->indiceNivel3[indiceV->numeroNodosNivel3].dato = a->dato;					// guardar el dato del nivel 3 en el indice3

	indiceV->indiceNivel3[indiceV->numeroNodosNivel3].lower = indiceV->numeroNodosNivel4;			// guardar lower indice3 --> contador nivel4

	Arbol arbol = (Arbol) a->siguiente_nivel;								// obtener el arbol de nivel4
	auxCrearNivel4_4(arbol, indiceV);										// recorrer el arbol de nivel4

	indiceV->indiceNivel3[indiceV->numeroNodosNivel3].upper = indiceV->numeroNodosNivel4 - 1;		// guardar upper indice3 ---> contador nivel4 - 1

	indiceV->numeroNodosNivel3++;										// avanzar el contador de datos del nivel3

	if(a->derecho)
		auxCrearNivel3_4(a->derecho, indiceV);
}

void auxCrearNivel2_4(Arbol a, IndiceVector4 indiceV) {
	if (a->izquierdo)
		auxCrearNivel2_4(a->izquierdo, indiceV);

	indiceV->indiceNivel2[indiceV->numeroNodosNivel2].dato = a->dato;					// guardar el dato del nivel 2 en el indice2

	indiceV->indiceNivel2[indiceV->numeroNodosNivel2].lower = indiceV->numeroNodosNivel3;			// guardar lower indice2 --> contador nivel3

	Arbol arbol = (Arbol) a->siguiente_nivel;								// obtener el arbol de nivel3
	auxCrearNivel3_4(arbol, indiceV);										// recorrer el arbol de nivel3

	indiceV->indiceNivel2[indiceV->numeroNodosNivel2].upper = indiceV->numeroNodosNivel3 - 1;		// guardar upper indice2 ---> contador nivel3 - 1

	indiceV->numeroNodosNivel2++;										// avanzar el contador de datos del nivel2

	if(a->derecho)
		auxCrearNivel2_4(a->derecho, indiceV);
}

void auxCrearNivel1_4(Arbol a, IndiceVector4 indiceV) {
	if (a->izquierdo)
		auxCrearNivel1_4(a->izquierdo, indiceV);

	indiceV->indiceNivel1[indiceV->numeroNodosNivel1].dato = a->dato;					// guardar el dato del nivel 1 en el indice1

	indiceV->indiceNivel1[indiceV->numeroNodosNivel1].lower = indiceV->numeroNodosNivel2;			// guardar lower indice1 --> contador nivel2

	Arbol arbol = (Arbol) a->siguiente_nivel;								// obtener el arbol de nivel2
	auxCrearNivel2_4(arbol, indiceV);										// recorrer el arbol de nivel2

	indiceV->indiceNivel1[indiceV->numeroNodosNivel1].upper = indiceV->numeroNodosNivel2 - 1;		// guardar upper indice1 ---> contador nivel2 - 1

	indiceV->numeroNodosNivel1++;										// avanzar el contador de datos del nivel1

	if(a->derecho)
		auxCrearNivel1_4(a->derecho, indiceV);
}

IndiceVector4 CrearIndiceVector_4(IndiceAVL4 indiceA) {

	IndiceVector4 indiceV = (pIndiceVector4) malloc(sizeof(tipoIndiceVector4));				// reservando el puntero para el indice vector

	indiceV->numeroNodosNivel1 = indiceA->numeroNodosNivel1;						// copiar el numero de nodos del nivel 1 del indice AVL al numero de nodos del nivel 1 del indice Vector

	indiceV->indiceNivel1 = (pIndicePlano) malloc(indiceV->numeroNodosNivel1 * sizeof(tipoIndicePlano));	// reserva de memoria para el vector de datos del indice Vector correspondiente al nivel 1

	indiceV->numeroNodosNivel2 = indiceA->numeroNodosNivel2;						// copiar el numero de nodos del nivel 2 del indice AVL al numero de nodos del nivel 2 del indice Vector

	indiceV->indiceNivel2 = (pIndicePlano) malloc(indiceV->numeroNodosNivel2 * sizeof(tipoIndicePlano));	// reserva de memoria para el vector de datos del indice Vector correspondiente al nivel 2

	indiceV->numeroNodosNivel3 = indiceA->numeroNodosNivel3;						// copiar el numero de nodos del nivel 3 del indice AVL al numero de nodos del nivel 3 del indice Vector

	indiceV->indiceNivel3 = (pIndicePlano) malloc(indiceV->numeroNodosNivel3 * sizeof(tipoIndicePlano));	// reserva de memoria para el vector de datos del indice Vector correspondiente al nivel 3

	indiceV->numeroNodosNivel4 = indiceA->numeroNodosNivel4;						// copiar el numero de nodos del nivel 4 del indice AVL al numero de nodos del nivel 4 del indice Vector

	indiceV->indiceNivel4 = (pIndicePlano) malloc(indiceV->numeroNodosNivel4 * sizeof(tipoIndicePlano));	// reserva de memoria para el vector de datos del indice Vector correspondiente al nivel 4

	indiceV->numeroEstrellasIndice = indiceA->numeroEstrellasIndice;					// copiar el numero de nodos IDs del indice AVL al numero de nodos IDs del indice Vector

	indiceV->vector_ids = (pID) malloc(indiceV->numeroEstrellasIndice * sizeof(ID));			// reserva de memoria para el vector IDs del indice Vector

	indiceV->numeroNodosNivel1 = 0;									// una vez se han realizado las reservas, ponemos los contadores a 0 para crear de forma adecuada
	indiceV->numeroNodosNivel2 = 0;									// la estructura indice Vector.
	indiceV->numeroNodosNivel3 = 0;
	indiceV->numeroNodosNivel4 = 0;
	indiceV->numeroEstrellasIndice = 0;

	auxCrearNivel1_4(indiceA->indice, indiceV);								// lanzamos el recorrido de la estructura que contiene la informacion del fichero crowding en
														// forma de arbol indexado de 4 niveles.

	if (!((indiceV->numeroNodosNivel1 == indiceA->numeroNodosNivel1) &&					// si el recorrido se hizo correctamente los cinco contadores de ambos indices deben ser iguales
	      (indiceV->numeroNodosNivel2 == indiceA->numeroNodosNivel2) &&
	      (indiceV->numeroNodosNivel3 == indiceA->numeroNodosNivel3) &&
	      (indiceV->numeroNodosNivel4 == indiceA->numeroNodosNivel4) &&
	      (indiceV->numeroEstrellasIndice == indiceA->numeroEstrellasIndice))) {
		printf("Error: Transformacion de indice AVL a indice Vector incorrecta...\n");
		exit(EXIT_FAILURE);
	}

	return indiceV;
}

/* Funciones para la creacion del indice Vector */


/* Funcion que libera la memoria del indice Vector */

void PodarIndiceVector_4(IndiceVector4 *indiceV) {
	free((*indiceV)->vector_ids);
	(*indiceV)->vector_ids = NULL;
	free((*indiceV)->indiceNivel1);
	(*indiceV)->indiceNivel1 = NULL;
	free((*indiceV)->indiceNivel2);
	(*indiceV)->indiceNivel2 = NULL;
	free((*indiceV)->indiceNivel3);
	(*indiceV)->indiceNivel3 = NULL;
	free((*indiceV)->indiceNivel4);
	(*indiceV)->indiceNivel4 = NULL;

	(*indiceV)->numeroNodosNivel1 = 0;
	(*indiceV)->numeroNodosNivel2 = 0;
	(*indiceV)->numeroNodosNivel3 = 0;
	(*indiceV)->numeroNodosNivel4 = 0;
	(*indiceV)->numeroEstrellasIndice = 0;

	free(*indiceV);
	*indiceV = NULL;
}

/* Funcion que libera la memoria del indice Vector */


/* Funcion de recorrido del indice */

void RecorrerIndiceVector_4(IndiceVector4 indiceV, int *contador, ListaDatosCrowding listCrowd, ListaDatosCrowding list) {
	*contador = 0;
	int i, j, k, l, m;
	for (i = 0; i < indiceV->numeroNodosNivel1; i++) {

		for (j = indiceV->indiceNivel1[i].lower; j <= indiceV->indiceNivel1[i].upper; j++) {

			for (k = indiceV->indiceNivel2[j].lower; k <= indiceV->indiceNivel2[j].upper; k++) {

				for (l = indiceV->indiceNivel3[k].lower; l <= indiceV->indiceNivel3[k].upper; l++) {

					for (m = indiceV->indiceNivel4[l].lower; m <= indiceV->indiceNivel4[l].upper; m++) {

						list[(*contador) * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_M1I] = listCrowd[indiceV->vector_ids[m] * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_M1I];
						list[(*contador) * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_M2I] = listCrowd[indiceV->vector_ids[m] * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_M2I];
						list[(*contador) * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_M1O] = listCrowd[indiceV->vector_ids[m] * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_M1O];
						list[(*contador) * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_M2O] = listCrowd[indiceV->vector_ids[m] * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_M2O];
						list[(*contador) * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_X] = listCrowd[indiceV->vector_ids[m] * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_X];
						list[(*contador) * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_Y] = listCrowd[indiceV->vector_ids[m] * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_Y];

						(*contador)++;
					}
				}
			}
		}
	}
}

/* Funcion de recorrido del indice */


/* Funcion de informacion del indice */

void InformacionIndiceVector_4(IndiceVector4 indiceV, int recorrer) {
	printf("Informacion general del indice:\n\n");
	printf("Numero de datos en el nivel 1: %d\n", indiceV->numeroNodosNivel1);
	printf("Numero de datos en el nivel 2: %d\n", indiceV->numeroNodosNivel2);
	printf("Numero de datos en el nivel 3: %d\n", indiceV->numeroNodosNivel3);
	printf("Numero de datos en el nivel 4: %d\n", indiceV->numeroNodosNivel4);
	printf("Numero de estrellas totales en el indice: %d\n", indiceV->numeroEstrellasIndice);

	printf("\n");

	if (recorrer) {

		printf("Informacion del indice en forma de estructura:\n\n");
		printf("Exp1\t\tExp2\t\tExp3\t\tExp4\t\tIDs\n");
		int i, j, k, l, m;
		for (i = 0; i < indiceV->numeroNodosNivel1; i++) {

			printf("%lf\n", indiceV->indiceNivel1[i].dato);

			for (j = indiceV->indiceNivel1[i].lower; j <= indiceV->indiceNivel1[i].upper; j++) {

				printf("\t\t%lf\n", indiceV->indiceNivel2[j].dato);

				for (k = indiceV->indiceNivel2[j].lower; k <= indiceV->indiceNivel2[j].upper; k++) {

					printf("\t\t\t\t%lf\n", indiceV->indiceNivel3[k].dato);
			
					for (l = indiceV->indiceNivel3[k].lower; l <= indiceV->indiceNivel3[k].upper; l++) {
					
						printf("\t\t\t\t\t\t%lf\n", indiceV->indiceNivel4[l].dato);

						printf("\t\t\t\t\t\t\t\t");

						for (m = indiceV->indiceNivel4[l].lower; m <= indiceV->indiceNivel4[l].upper; m++) {

							printf("%d", indiceV->vector_ids[m]);
							if (m < indiceV->indiceNivel4[l].upper) {
								printf(", ");
							}
						}

						printf("\n");
					}
				}
			}

		}

		printf("\n");

		printf("Informacion por vectores del indice:\n\n");

		printf("Vector Nivel 1:\n");
		for (i = 0; i < indiceV->numeroNodosNivel1; i++) {
			printf("%lf [%d, %d]", indiceV->indiceNivel1[i].dato, indiceV->indiceNivel1[i].lower, indiceV->indiceNivel1[i].upper);
			if (i < (indiceV->numeroNodosNivel1 - 1)) {
				printf(" - ");
			}
		}

		printf("\n\n");

		printf("Vector Nivel 2:\n");
		for (i = 0; i < indiceV->numeroNodosNivel2; i++) {
			printf("%lf [%d, %d]", indiceV->indiceNivel2[i].dato, indiceV->indiceNivel2[i].lower, indiceV->indiceNivel2[i].upper);
			if (i < (indiceV->numeroNodosNivel2 - 1)) {
				printf(" - ");
			}
		}

		printf("\n\n");

		printf("Vector Nivel 3:\n");
		for (i = 0; i < indiceV->numeroNodosNivel3; i++) {
			printf("%lf [%d, %d]", indiceV->indiceNivel3[i].dato, indiceV->indiceNivel3[i].lower, indiceV->indiceNivel3[i].upper);
			if (i < (indiceV->numeroNodosNivel3 - 1)) {
				printf(" - ");
			}
		}

		printf("\n\n");

		printf("Vector Nivel 4:\n");
		for (i = 0; i < indiceV->numeroNodosNivel4; i++) {
			printf("%lf [%d, %d]", indiceV->indiceNivel4[i].dato, indiceV->indiceNivel4[i].lower, indiceV->indiceNivel4[i].upper);
			if (i < (indiceV->numeroNodosNivel4 - 1)) {
				printf(" - ");
			}
		}

		printf("\n\n");

		printf("Vector IDs:\n");
		for (i = 0; i < indiceV->numeroEstrellasIndice; i++) {
			printf("%d ", indiceV->vector_ids[i]);
		}

		printf("\n\n");
	}
}

/* Funcion de informacion del indice */


/* Funcion de busqueda */

void BuscarEstrellasIndiceVector_4(IndiceVector4 indiceV, tipoDato *rangos, int *contador, ListaID list, int estrellaDispersoEntero, ListaDatosDispersoEntero *listaDatosDispersoEntero) {

	*contador = 0;

	int lower_index_1 = busquedaBinariaIndiceVectorInferior(indiceV->indiceNivel1, 0, indiceV->numeroNodosNivel1 - 1, rangos[ERROR_DATO_1_MENOS]);
	int upper_index_1 = busquedaBinariaIndiceVectorSuperior(indiceV->indiceNivel1, 0, indiceV->numeroNodosNivel1 - 1, rangos[ERROR_DATO_1_MAS]);

	if (lower_index_1 >= 0 && upper_index_1 >= 0) {

		int i, j, k, l, m;

		for (i = lower_index_1; i <= upper_index_1; i++) {

			int lower_index_2 = busquedaBinariaIndiceVectorInferior(indiceV->indiceNivel2, indiceV->indiceNivel1[i].lower, indiceV->indiceNivel1[i].upper, rangos[ERROR_DATO_2_MENOS]);
			int upper_index_2 = busquedaBinariaIndiceVectorSuperior(indiceV->indiceNivel2, indiceV->indiceNivel1[i].lower, indiceV->indiceNivel1[i].upper, rangos[ERROR_DATO_2_MAS]);

			if (lower_index_2 >= 0 && upper_index_2 >= 0) {

				for (j = lower_index_2; j <= upper_index_2; j++) {

					int lower_index_3 = busquedaBinariaIndiceVectorInferior(indiceV->indiceNivel3, indiceV->indiceNivel2[j].lower, indiceV->indiceNivel2[j].upper, rangos[ERROR_DATO_3_MENOS]);
					int upper_index_3 = busquedaBinariaIndiceVectorSuperior(indiceV->indiceNivel3, indiceV->indiceNivel2[j].lower, indiceV->indiceNivel2[j].upper, rangos[ERROR_DATO_3_MAS]);

					if (lower_index_3 >= 0 && upper_index_3 >= 0) {

						for (k = lower_index_3; k <= upper_index_3; k++) {

							int lower_index_4 = busquedaBinariaIndiceVectorInferior(indiceV->indiceNivel4, indiceV->indiceNivel3[k].lower, indiceV->indiceNivel3[k].upper, rangos[ERROR_DATO_4_MENOS]);
							int upper_index_4 = busquedaBinariaIndiceVectorSuperior(indiceV->indiceNivel4, indiceV->indiceNivel3[k].lower, indiceV->indiceNivel3[k].upper, rangos[ERROR_DATO_4_MAS]);

							if (lower_index_4 >= 0 && upper_index_4 >= 0) {

								for (l = lower_index_4; l <= upper_index_4; l++) {

									for (m = indiceV->indiceNivel4[l].lower; m <= indiceV->indiceNivel4[l].upper; m++) {

										if ((*contador) < MAX_ESTRELLAS_SELECCIONADAS) {
											list[(*contador)] = indiceV->vector_ids[m];
											(*contador)++;
										}
										else {
											(*listaDatosDispersoEntero[estrellaDispersoEntero + COL_DISPERSO_OVER_BUFFER]) = 1;
											return;
										}

									}
								}
							}
						}
					}
				}
			}
		}
	}
}

/* Funcion de busqueda */

