#ifndef SALIDA_CROWDING2_H
#define SALIDA_CROWDING2_H

#include <stdlib.h>
#include <stdio.h>

#include "tipos2.h"

void imprimir_fichero_crowding_ordenado_con_anotaciones_indice_vector_2(ListaDatosCrowding listCrowd, IndiceVector2 indiceV2, ListaUsoEstrellasCrowding listaUsoEstrellasCrowding, ParametrosEjecucion parametrosEjecucion);

#endif