#include "parametros_entrada.h"

// Funcion que lee el fichero de parametros
ParametrosEjecucion leerParametrosEntrada(int nparam, char *argv[]) {

	ParametrosEjecucion parametrosEjecucion;
	parametrosEjecucion.numIndices = 0;
	memset(parametrosEjecucion.fichMadre, '\0', MAX_TAM_BUFFER);
	memset(parametrosEjecucion.fichCrowdingEntrada, '\0', MAX_TAM_BUFFER);
	memset(parametrosEjecucion.fichDisperso, '\0', MAX_TAM_BUFFER);
	memset(parametrosEjecucion.fichCrowdingSalida, '\0', MAX_TAM_BUFFER);
	memset(parametrosEjecucion.fichSelecciones, '\0', MAX_TAM_BUFFER);
	parametrosEjecucion.numIndices = 0;
	parametrosEjecucion.exps_madre = NULL;
	parametrosEjecucion.exps_crowding = NULL;
	parametrosEjecucion.epsilons = NULL;
	parametrosEjecucion.tolerancia = 0.0;
	parametrosEjecucion.numMinimoEstrellas = 0;
	parametrosEjecucion.tamBufferMadres = 0;
	parametrosEjecucion.device = -1;
	parametrosEjecucion.threads_per_block = 0;
	parametrosEjecucion.num_blocks = 0;
	parametrosEjecucion.totalMadre = 0;
	parametrosEjecucion.totalCrowding = 0;

	if (nparam != 2) {
		parametrosEjecucion.codSalida = -1;
		return parametrosEjecucion;
	}

	FILE *fparametros;
	fparametros = fopen(argv[1], "r");

	if (fparametros == NULL) {
		parametrosEjecucion.codSalida = -2;
		return parametrosEjecucion;
	}
	else {

		char linea[MAX_TAM_BUFFER];

		char parametros[MAX_LINEAS_FICHERO_PARAMETROS][MAX_TAM_BUFFER];

		int i = 1;
		while (!feof(fparametros)) {

			memset(linea, '\0', MAX_TAM_BUFFER);

			fgets(linea, MAX_TAM_BUFFER, fparametros);

			// Extraer el parametro del fichero de parametros (ignorando comentarios)
			int j;
			for (j = 0; (j < MAX_TAM_BUFFER && linea[j] != '\0' && linea[j] != '\t' && linea[j] != ' ' && linea[j] != 10 && linea[j] != 13); j++)
				parametros[i-1][j] = linea[j];
			parametros[i-1][j] = '\0';

			// Alguno de los parametros no esta
			if (j == 0 && i <= MAX_LINEAS_FICHERO_PARAMETROS) {
				parametrosEjecucion.codSalida = -3;
				return parametrosEjecucion;
			}

			// Ignoramos las lineas que no son parametros
			if (j >= 0 && i > MAX_LINEAS_FICHERO_PARAMETROS) {
				continue;
			}

			i++;

		}


		if ((i - 1)  < MAX_LINEAS_FICHERO_PARAMETROS) {
			parametrosEjecucion.codSalida = -3;
			return parametrosEjecucion;
		}

		for (i = 0; i < MAX_LINEAS_FICHERO_PARAMETROS; i++) {

			if (i >= 0 && i <= 4) {
				if (i == 0)
					memcpy(parametrosEjecucion.fichMadre, parametros[i], MAX_TAM_BUFFER);
				else if (i == 1)
					memcpy(parametrosEjecucion.fichCrowdingEntrada, parametros[i], MAX_TAM_BUFFER);
				else if (i == 2)
					memcpy(parametrosEjecucion.fichDisperso, parametros[i], MAX_TAM_BUFFER);
				else if (i == 3)
					memcpy(parametrosEjecucion.fichCrowdingSalida, parametros[i], MAX_TAM_BUFFER);
				else
					memcpy(parametrosEjecucion.fichSelecciones, parametros[i], MAX_TAM_BUFFER);
			}
			else if (i == 5) {

				if (strlen(parametros[i]) > 1) {
					parametrosEjecucion.codSalida = -3;
					return parametrosEjecucion;
				}


				if (parametros[i][0] == '2' || parametros[i][0] == '4') {
					parametrosEjecucion.numIndices = atoi(parametros[i]);

					int j;
					parametrosEjecucion.exps_madre = (char **)malloc(MAX_NUM_INDICES * sizeof(char*));
					for (j = 0; j < MAX_NUM_INDICES; j++)
						parametrosEjecucion.exps_madre[j] = (char *)malloc(MAX_TAM_BUFFER * sizeof(char));

					parametrosEjecucion.exps_crowding = (char **)malloc(MAX_NUM_INDICES * sizeof(char*));
					for (j = 0; j < MAX_NUM_INDICES; j++)
						parametrosEjecucion.exps_crowding[j] = (char *)malloc(MAX_TAM_BUFFER * sizeof(char));

					parametrosEjecucion.epsilons = (tipoDato *)malloc(MAX_NUM_INDICES * sizeof(tipoDato));
				}
				else {
					parametrosEjecucion.codSalida = -3;
					return parametrosEjecucion;
				}
			}
			else if (i >= 6 && i <= 13) {
				int j;
				for (j = 0; (j < MAX_TAM_BUFFER && parametros[i][j] != '\0'); j++)
					if (!(parametros[i][j] >= '1' && parametros[i][j] <= '6') && (parametros[i][j] != '(') && (parametros[i][j] != ')') && (parametros[i][j] != 'c') && (parametros[i][j] != 'C') &&
					     (parametros[i][j] != '+') && (parametros[i][j] != '-') && (parametros[i][j] != '*') && (parametros[i][j] != '/') && (parametros[i][j] != ' ')) {
						parametrosEjecucion.codSalida = -3;
						return parametrosEjecucion;
					}

				if (i == 6)
					memcpy(parametrosEjecucion.exps_madre[0], parametros[i], MAX_TAM_BUFFER);
				else if (i == 7)
					memcpy(parametrosEjecucion.exps_madre[1], parametros[i], MAX_TAM_BUFFER);
				else if (i == 8)
					memcpy(parametrosEjecucion.exps_madre[2], parametros[i], MAX_TAM_BUFFER);
				else if (i == 9)
					memcpy(parametrosEjecucion.exps_madre[3], parametros[i], MAX_TAM_BUFFER);
				else if (i == 10)
					memcpy(parametrosEjecucion.exps_crowding[0], parametros[i], MAX_TAM_BUFFER);
				else if (i == 11)
					memcpy(parametrosEjecucion.exps_crowding[1], parametros[i], MAX_TAM_BUFFER);
				else if (i == 12)
					memcpy(parametrosEjecucion.exps_crowding[2], parametros[i], MAX_TAM_BUFFER);
				else
					memcpy(parametrosEjecucion.exps_crowding[3], parametros[i], MAX_TAM_BUFFER);
			}
			else if (i >= 14 && i <= 18) {
				int j;
				for (j = 0; (j < MAX_TAM_BUFFER && parametros[i][j] != '\0'); j++)
					if (!(parametros[i][j] >= '0' && parametros[i][j] <= '9') && parametros[i][j] != '.' && parametros[i][j] != 'e' && parametros[i][j] != 'E' && parametros[i][j] != '+' && parametros[i][j] != '-') {
						parametrosEjecucion.codSalida = -3;
						return parametrosEjecucion;
					}

				tipoDato valor = atof(parametros[i]);
				if (i == 14)
					parametrosEjecucion.epsilons[0] = valor;
				else if (i == 15)
					parametrosEjecucion.epsilons[1] = valor;
				else if (i == 16)
					parametrosEjecucion.epsilons[2] = valor;
				else if (i == 17)
					parametrosEjecucion.epsilons[3] = valor;
				else
					parametrosEjecucion.tolerancia = valor;
			}
			else {
				int j;
				for (j = 0; (j < MAX_TAM_BUFFER && parametros[i][j] != '\0'); j++)
					if (!(parametros[i][j] >= '0' && parametros[i][j] <= '9')) {
						parametrosEjecucion.codSalida = -3;
						return parametrosEjecucion;
					}
				int valor = atoi(parametros[i]);
				if (i == 19)
					parametrosEjecucion.numMinimoEstrellas = valor;
				else if (i == 20)
					parametrosEjecucion.tamBufferMadres = valor;
				else if (i == 21)
					parametrosEjecucion.device = valor;
				else if (i == 22)
					parametrosEjecucion.threads_per_block = valor;
				else
					parametrosEjecucion.num_blocks = valor;
			}

		}

		parametrosEjecucion.codSalida = 1;
		return parametrosEjecucion;
	}
}

// Funcion para mostrar los parametros cargados
void mostrarParametrosEntrada(ParametrosEjecucion parametrosEjecucion) {
	printf("Parametros\n");
	printf("Linea 1 -------- Fichero Madre\t\t\t\t\t\t\t\t%s\n", parametrosEjecucion.fichMadre);
	printf("Linea 2 -------- Fichero Crowding\t\t\t\t\t\t\t%s\n", parametrosEjecucion.fichCrowdingEntrada);
	printf("Linea 3 -------- Fichero Disperso\t\t\t\t\t\t\t%s\n", parametrosEjecucion.fichDisperso);
	printf("Linea 4 -------- Fichero Crowding de Salida\t\t\t\t\t\t%s\n", parametrosEjecucion.fichCrowdingSalida);
	printf("Linea 5 -------- Fichero de Selecciones\t\t\t\t\t\t\t%s\n", parametrosEjecucion.fichSelecciones);
	printf("Linea 6 -------- Numero de rangos\t\t\t\t\t\t\t%d\n", parametrosEjecucion.numIndices);
	printf("Linea 7 -------- Expresion Rango 1 (estrellas madre)\t\t\t\t\t%s\n", parametrosEjecucion.exps_madre[0]);
	printf("Linea 8 -------- Expresion Rango 2 (estrellas madre)\t\t\t\t\t%s\n", parametrosEjecucion.exps_madre[1]);
	printf("Linea 9 -------- Expresion Rango 3 (estrellas madre)\t\t\t\t\t%s\n", parametrosEjecucion.exps_madre[2]);
	printf("Linea 10 ------- Expresion Rango 4 (estrellas madre)\t\t\t\t\t%s\n", parametrosEjecucion.exps_madre[3]);
	printf("Linea 11 ------- Expresion Rango 1 (estrellas crowding)\t\t\t\t\t%s\n", parametrosEjecucion.exps_crowding[0]);
	printf("Linea 12 ------- Expresion Rango 2 (estrellas crowding)\t\t\t\t\t%s\n", parametrosEjecucion.exps_crowding[1]);
	printf("Linea 13 ------- Expresion Rango 3 (estrellas crowding)\t\t\t\t\t%s\n", parametrosEjecucion.exps_crowding[2]);
	printf("Linea 14 ------- Expresion Rango 4 (estrellas crowding)\t\t\t\t\t%s\n", parametrosEjecucion.exps_crowding[3]);
	printf("Linea 15 ------- Epsilon Error Rango 1\t\t\t\t\t\t\t%lf\n", parametrosEjecucion.epsilons[0]);
	printf("Linea 16 ------- Epsilon Error Rango 2\t\t\t\t\t\t\t%lf\n", parametrosEjecucion.epsilons[1]);
	printf("Linea 17 ------- Epsilon Error Rango 3\t\t\t\t\t\t\t%lf\n", parametrosEjecucion.epsilons[2]);
	printf("Linea 18 ------- Epsilon Error Rango 4\t\t\t\t\t\t\t%lf\n", parametrosEjecucion.epsilons[3]);
	printf("Linea 19 ------- Tolerancia\t\t\t\t\t\t\t\t%lf\n", parametrosEjecucion.tolerancia);
	printf("Linea 20 ------- Numero minimo de estrellas a seleccionar\t\t\t\t%d\n", parametrosEjecucion.numMinimoEstrellas);
	printf("Linea 21 ------- CUDA Tamanio Lote de Estrellas Madre a Procesar\t\t\t%d\n", parametrosEjecucion.tamBufferMadres);
	printf("Linea 22 ------- CUDA Device GPU ID donde realizaremos la ejecucion\t\t\t%d\n", parametrosEjecucion.device);
	printf("Linea 23 ------- CUDA Numero de threads por bloque\t\t\t\t\t%d\n", parametrosEjecucion.threads_per_block);
	printf("Linea 24 ------- CUDA Numero de bloques\t\t\t\t\t\t\t%d\n", parametrosEjecucion.num_blocks);
	printf("\n\n");
	printf("Codigo que indica el estado de la lectura: (1)Exito | (<0)Fracaso\t\t\t%d\n", parametrosEjecucion.codSalida);
}

void erroresParametrosEntrada(ParametrosEjecucion parametrosEjecucion, char *nombre_fichero) {
	if (parametrosEjecucion.codSalida < 0) {
		if (parametrosEjecucion.codSalida == -1) {
			printf("Error: Numero de parametros de entrada incorrecto...\n");
			printf("\n");
			printf("\tUso correcto:\n");
			printf("\t\t$ ./<ejecutable> <fichero-parametros>\n");
		} else if (parametrosEjecucion.codSalida == -2) {
			printf("Error: El fichero de parametros %s no existe en el sistema de ficheros...\n", nombre_fichero);
		} else if (parametrosEjecucion.codSalida == -3) {
			printf("Error: El formato del fichero de parametros %s no es el correcto...\n", nombre_fichero);
			printf("\n");
			printf("\tEl formato del fichero de parametros es el siguiente:\n");
			printf("\t\tLinea 1: Nombre del fichero madre (cadena de caracteres).\n");
			printf("\t\tLinea 2: Nombre del fichero crowding (cadena de caracteres).\n");
			printf("\t\tLinea 3: Nombre del fichero salida [fichero disperso] (cadena de caracteres).\n");
			printf("\t\tLinea 4: Nombre del fichero crowding de salida con anotaciones de uso de estrellas crowding (cadena de caracteres).\n");
			printf("\t\tLinea 5: Nombre del fichero selecciones [fichero selecciones] (cadena de caracteres).\n");
			printf("\t\tLinea 6: Numero de rangos de busqueda (numero entero positivo, 2 o 4).\n");
			printf("\t\tLinea 7: Expresion a utilizar en el primer rango de busqueda (estrellas madre) (cadena de caracteres).\n");
			printf("\t\tLinea 8: Expresion a utilizar en el segundo rango de busqueda (estrellas madre) (cadena de caracteres).\n");
			printf("\t\tLinea 9: Expresion a utilizar en el tercer rango de busqueda (estrellas madre) (cadena de caracteres).\n");
			printf("\t\tLinea 10: Expresion a utilizar en el cuarto rango de busqueda (estrellas madre) (cadena de caracteres).\n");
			printf("\t\tLinea 11: Expresion a utilizar en el primer rango de busqueda (estrellas crowding) (cadena de caracteres).\n");
			printf("\t\tLinea 12: Expresion a utilizar en el segundo rango de busqueda (estrellas crowding) (cadena de caracteres).\n");
			printf("\t\tLinea 13: Expresion a utilizar en el tercer rango de busqueda (estrellas crowding) (cadena de caracteres).\n");
			printf("\t\tLinea 14: Expresion a utilizar en el cuarto rango de busqueda (estrellas crowding) (cadena de caracteres).\n");
			printf("\t\tLinea 15: Epsilon del error aplicado en el primer rango de busqueda (numero real).\n");
			printf("\t\tLinea 16: Epsilon del error aplicado en el segundo rango de busqueda (numero real).\n");
			printf("\t\tLinea 17: Epsilon del error aplicado en el tercer rango de busqueda (numero real).\n");
			printf("\t\tLinea 18: Epsilon del error aplicado en el cuarto rango de busqueda (numero real).\n");
			printf("\t\tLinea 19: Tolerancia para M1o y M2o (numero real).\n");
			printf("\t\tLinea 20: Numero minimo de estrellas que deben seleccionarse (numero entero positivo).\n");
			printf("\t\tLinea 21: Tamanio del buffer de estrellas madres a procesar (numero entero positivo).\n");
			printf("\t\tLinea 22: Dispositivo donde se ejecutara el procesado de los buffer de las estrellas madres (numero entero positivo).\n");
			printf("\t\tLinea 23: Numero de threads por bloque (numero entero positivo).\n");
			printf("\t\tLinea 24: Numero de bloques (numero entero positivo).\n");
			printf("\n");
			printf("NOTAS: Cada linea debe terminar con un retorno de carro (ENTER).\n");
			printf("       Para los numeros reales, usar el punto como separador entre la parte entera y decimal.\n");
			printf("       Se permite la notacion cientifica para expresar exponentes, con el caracter e o E, un + o un -, seguido de un numero entero positivo.\n");
			printf("       Se aceptan comentarios despues de cada parametro. Usar como separador entre parametro y comentario un espacio en blanco o un tabulador.\n");
		}
		exit(EXIT_FAILURE);
	}

	if (parametrosEjecucion.numMinimoEstrellas > MAX_ESTRELLAS_SELECCIONADAS) {
		printf("Error: Numero minimo de estrellas a seleccionar y tamanio del buffer de selecciones INCORRECTO\n");
		printf("\n");
		printf("\t\tNumero minimo de estrellas a seleccionar: Linea 20 del fichero de parametros\n");
		printf("\t\tTamanio del buffer de selecciones: Definido por la constante MAX_ESTRELLAS_SELECCIONADAS en el Makefile\n");
		printf("\n");
		printf("\t\tPara solucionar este error debe cumplirse lo siguiente:\n");
		printf("\n");
		printf("\t\t\t------> numero_minimo_Estrellas <= MAX_ESTRELLAS_SELECCIONADAS <------\n");
		printf("\n");
		exit(EXIT_FAILURE);
	}
}

