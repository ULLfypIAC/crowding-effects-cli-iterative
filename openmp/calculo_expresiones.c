#include "calculo_expresiones.h"

void calculoExprCrowding(ListaDatosCrowding listaDatosCrowding, ParametrosEjecucion parametrosEjecucion, ListaDatosCrowdingExp listaDatosCrowdingExp) {

	int i, j, k, l;

	char columna[MAX_TAM_BUFFER];
	int columna_int;

	void *f_expr;
	int numCols;
	char **nombresColumnas;
	int *columnas;
	double *valores;

	tipoDato **datos_aux;
	datos_aux = (tipoDato **)malloc(parametrosEjecucion.numIndices * sizeof(tipoDato*));
	for (i = 0; i < parametrosEjecucion.numIndices; i++)
		datos_aux[i] = (tipoDato *)malloc(parametrosEjecucion.totalCrowding * sizeof(tipoDato));

	for (i = 0; i < parametrosEjecucion.numIndices; i++) {
		// Creando el evaluador para la expresion
		f_expr = evaluator_create(parametrosEjecucion.exps_crowding[i]);
		// Obtener el numero de columnas de la expresion y sus nombres
		evaluator_get_variables(f_expr, &nombresColumnas, &numCols);
		// Inicializando array de valores y array para los numeros de columnas
		valores = (double *) malloc(numCols * sizeof(double));
		columnas = (int *) malloc(numCols * sizeof(int));

		// Obteniendo las columnas que son usadas en la expresion
		for (j = 0; j < numCols; j++) {
			memset(columna, '\0', MAX_TAM_BUFFER);
			for (k = 1; k < strlen(nombresColumnas[j]); k++)
				columna[k - 1] = nombresColumnas[j][k];
			columna_int = atoi(columna);
			columnas[j] = columna_int - 1;
		}

		// Calculando el valor de la expresion para cada fila del fichero crowding.
		//	Rellenamos el array con los valores correspondiente y llamamos al evaluador
		//		NOTA: Una fila de la matriz datos_aux se corresponde con el calculo de una expresion  
		for (j = 0; j < parametrosEjecucion.totalCrowding; j++) {
			for (k = 0; k < numCols; k++)
				valores[k] = listaDatosCrowding[j * NUMERO_COLUMNAS_CROWDING + columnas[k]];
			datos_aux[i][j] = evaluator_evaluate(f_expr, numCols, nombresColumnas, valores);
		}

		// Liberar memoria de la matriz de nombres de columnas, el array de valores y el array de columnas
		for (j = 0; j < numCols; j++)
			free(nombresColumnas[j]);
		free(nombresColumnas);
		free(valores);
		free(columnas);

	}

	// Transposicion de la matriz de datos auxiliares
	l = 0;
	for (i = 0; i < parametrosEjecucion.totalCrowding; i++)
		for (j = 0; j < parametrosEjecucion.numIndices; j++) {
			listaDatosCrowdingExp[l] = datos_aux[j][i];
			l++;
		}

	// Liberar la memoria de la matriz auxiliar
	for (i = 0; i < parametrosEjecucion.numIndices; i++)
		free(datos_aux[i]);
	free(datos_aux);
}


void copiarDatosCrowdingParcial(ListaDatosCrowding listaDatosCrowding, ParametrosEjecucion parametrosEjecucion, ListaDatosCrowdingParcial listaDatosCrowdingParcial) {
	int i;
	for (i = 0; i < parametrosEjecucion.totalCrowding; i++) {
		listaDatosCrowdingParcial[i * NUMERO_COLUMNAS_CROWDING_PARCIAL + COL_CROWDING_PARCIAL_M1I] = listaDatosCrowding[i * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_M1I];
		listaDatosCrowdingParcial[i * NUMERO_COLUMNAS_CROWDING_PARCIAL + COL_CROWDING_PARCIAL_M2I] = listaDatosCrowding[i * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_M2I];
		listaDatosCrowdingParcial[i * NUMERO_COLUMNAS_CROWDING_PARCIAL + COL_CROWDING_PARCIAL_M1O] = listaDatosCrowding[i * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_M1O];
		listaDatosCrowdingParcial[i * NUMERO_COLUMNAS_CROWDING_PARCIAL + COL_CROWDING_PARCIAL_M2O] = listaDatosCrowding[i * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_M2O];
	}
}

void calculoExprMadre(ListaDatosMadre listaDatosMadre, ParametrosEjecucion parametrosEjecucion, ListaDatosMadreExp listaDatosMadreExp) {

	int i, j, k, l;

	char columna[MAX_TAM_BUFFER];
	int columna_int;

	void *f_expr;
	int numCols;
	char **nombresColumnas;
	int *columnas;
	double *valores;

	tipoDato **datos_aux;
	datos_aux = (tipoDato **)malloc(parametrosEjecucion.numIndices * sizeof(tipoDato*));
	for (i = 0; i < parametrosEjecucion.numIndices; i++)
		datos_aux[i] = (tipoDato *)malloc(parametrosEjecucion.totalMadre * sizeof(tipoDato));

	for (i = 0; i < parametrosEjecucion.numIndices; i++) {
		// Creando el evaluador para la expresion
		f_expr = evaluator_create(parametrosEjecucion.exps_madre[i]);
		// Obtener el numero de columnas de la expresion y sus nombres
		evaluator_get_variables(f_expr, &nombresColumnas, &numCols);
		// Inicializando array de valores y array para los numeros de columnas
		valores = (double *) malloc(numCols * sizeof(double));
		columnas = (int *) malloc(numCols * sizeof(int));

		// Obteniendo las columnas que son usadas en la expresion
		for (j = 0; j < numCols; j++) {
			memset(columna, '\0', MAX_TAM_BUFFER);
			for (k = 1; k < strlen(nombresColumnas[j]); k++)
				columna[k - 1] = nombresColumnas[j][k];
			columna_int = atoi(columna);
			columnas[j] = columna_int - 1;
		}

		// Calculando el valor de la expresion para cada fila del fichero crowding.
		//	Rellenamos el array con los valores correspondiente y llamamos al evaluador
		//		NOTA: Una fila de la matriz datos_aux se corresponde con el calculo de una expresion  
		for (j = 0; j < parametrosEjecucion.totalMadre; j++) {
			for (k = 0; k < numCols; k++)
				valores[k] = listaDatosMadre[j * NUMERO_COLUMNAS_MADRE + columnas[k]];
			datos_aux[i][j] = evaluator_evaluate(f_expr, numCols, nombresColumnas, valores);
		}

		// Liberar memoria de la matriz de nombres de columnas, el array de valores y el array de columnas
		for (j = 0; j < numCols; j++)
			free(nombresColumnas[j]);
		free(nombresColumnas);
		free(valores);
		free(columnas);

	}

	// Transposicion de la matriz de datos auxiliares
	l = 0;
	for (i = 0; i < parametrosEjecucion.totalMadre; i++)
		for (j = 0; j < parametrosEjecucion.numIndices; j++) {
			listaDatosMadreExp[l] = datos_aux[j][i];
			l++;
		}

	// Liberar la memoria de la matriz auxiliar
	for (i = 0; i < parametrosEjecucion.numIndices; i++)
		free(datos_aux[i]);
	free(datos_aux);
}

