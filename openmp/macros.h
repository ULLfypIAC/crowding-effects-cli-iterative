#ifndef MACROS_H
#define MACROS_H

#define mayor(A, B) \
	({ \
		((A - B) > EPSILON); \
	})

#define menor(A, B) \
	({ \
		((A - B) < MENOS_EPSILON); \
	})

#define igual(A, B) \
	({ \
		((A - B) < EPSILON) && ((A - B) > MENOS_EPSILON); \
	})

#endif