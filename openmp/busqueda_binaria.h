#ifndef BUSQUEDA_BINARIA_H
#define BUSQUEDA_BINARIA_H

#include "tipos.h"
#include "macros.h"

int busquedaBinariaIndiceVectorInferior(ListaIndicePlano lista, int lower, int upper, tipoDato dato);
int busquedaBinariaIndiceVectorSuperior(ListaIndicePlano lista, int lower, int upper, tipoDato dato);

#endif