#include "indice_vector2.h"


/* Funciones para la creacion del indice Vector */

void auxCrearNivelLista_2(ListaNodosEspeciales lista, IndiceVector2 indiceV) {
	pNodoEspecial aux;
	aux = lista;
	while (aux != NULL) {
		indiceV->vector_ids[indiceV->numeroEstrellasIndice] = aux->id;
		indiceV->numeroEstrellasIndice++;
		aux = aux->siguiente;
	}
}

void auxCrearNivel2_2(Arbol a, IndiceVector2 indiceV) {
	if (a->izquierdo)
		auxCrearNivel2_2(a->izquierdo, indiceV);

	indiceV->indiceNivel2[indiceV->numeroNodosNivel2].dato = a->dato;					// guardar el dato del nivel 2 en el indice2

	indiceV->indiceNivel2[indiceV->numeroNodosNivel2].lower = indiceV->numeroEstrellasIndice;		// guardar lower indice2 --> contador vectorIds

	ListaNodosEspeciales lista = (pNodoEspecial) a->siguiente_nivel;					// obtener la lista de IDs
	auxCrearNivelLista_2(lista, indiceV);									// recorrer la lista de IDs

	indiceV->indiceNivel2[indiceV->numeroNodosNivel2].upper = indiceV->numeroEstrellasIndice - 1;		// guardar upper indice2 ---> contador vectorIds - 1

	indiceV->numeroNodosNivel2++;										// avanzar el contador de datos del nivel2

	if(a->derecho)
		auxCrearNivel2_2(a->derecho, indiceV);
}

void auxCrearNivel1_2(Arbol a, IndiceVector2 indiceV) {
	if (a->izquierdo)
		auxCrearNivel1_2(a->izquierdo, indiceV);

	indiceV->indiceNivel1[indiceV->numeroNodosNivel1].dato = a->dato;					// guardar el dato del nivel 1 en el indice1

	indiceV->indiceNivel1[indiceV->numeroNodosNivel1].lower = indiceV->numeroNodosNivel2;			// guardar lower indice1 --> contador nivel2

	Arbol arbol = (Arbol) a->siguiente_nivel;								// obtener el arbol de nivel2
	auxCrearNivel2_2(arbol, indiceV);										// recorrer el arbol de nivel2

	indiceV->indiceNivel1[indiceV->numeroNodosNivel1].upper = indiceV->numeroNodosNivel2 - 1;		// guardar upper indice1 ---> contador nivel2 - 1

	indiceV->numeroNodosNivel1++;										// avanzar el contador de datos del nivel1

	if(a->derecho)
		auxCrearNivel1_2(a->derecho, indiceV);
}

IndiceVector2 CrearIndiceVector_2(IndiceAVL2 indiceA) {

	IndiceVector2 indiceV = (pIndiceVector2) malloc(sizeof(tipoIndiceVector2));				// reservando el puntero para el indice vector

	indiceV->numeroNodosNivel1 = indiceA->numeroNodosNivel1;						// copiar el numero de nodos del nivel 1 del indice AVL al numero de nodos del nivel 1 del indice Vector

	indiceV->indiceNivel1 = (pIndicePlano) malloc(indiceV->numeroNodosNivel1 * sizeof(tipoIndicePlano));	// reserva de memoria para el vector de datos del indice Vector correspondiente al nivel 1

	indiceV->numeroNodosNivel2 = indiceA->numeroNodosNivel2;						// copiar el numero de nodos del nivel 2 del indice AVL al numero de nodos del nivel 2 del indice Vector

	indiceV->indiceNivel2 = (pIndicePlano) malloc(indiceV->numeroNodosNivel2 * sizeof(tipoIndicePlano));	// reserva de memoria para el vector de datos del indice Vector correspondiente al nivel 2

	indiceV->numeroEstrellasIndice = indiceA->numeroEstrellasIndice;					// copiar el numero de nodos IDs del indice AVL al numero de nodos IDs del indice Vector

	indiceV->vector_ids = (pID) malloc(indiceV->numeroEstrellasIndice * sizeof(ID));			// reserva de memoria para el vector IDs del indice Vector

	indiceV->numeroNodosNivel1 = 0;									// una vez se han realizado las reservas, ponemos los contadores a 0 para crear de forma adecuada
	indiceV->numeroNodosNivel2 = 0;									// la estructura indice Vector.
	indiceV->numeroEstrellasIndice = 0;

	auxCrearNivel1_2(indiceA->indice, indiceV);								// lanzamos el recorrido de la estructura que contiene la informacion del fichero crowding en
														// forma de arbol indexado de 2 niveles.

	if (!((indiceV->numeroNodosNivel1 == indiceA->numeroNodosNivel1) &&					// si el recorrido se hizo correctamente los tres contadores de ambos indices deben ser iguales
	      (indiceV->numeroNodosNivel2 == indiceA->numeroNodosNivel2) &&
	      (indiceV->numeroEstrellasIndice == indiceA->numeroEstrellasIndice))) {
		printf("Error: Transformacion de indice AVL a indice Vector incorrecta...\n");
		exit(EXIT_FAILURE);
	}

	return indiceV;
}

/* Funciones para la creacion del indice Vector */


/* Funcion que libera la memoria del indice Vector */

void PodarIndiceVector_2(IndiceVector2 *indiceV) {
	free((*indiceV)->vector_ids);
	(*indiceV)->vector_ids = NULL;
	free((*indiceV)->indiceNivel1);
	(*indiceV)->indiceNivel1 = NULL;
	free((*indiceV)->indiceNivel2);
	(*indiceV)->indiceNivel2 = NULL;

	(*indiceV)->numeroNodosNivel1 = 0;
	(*indiceV)->numeroNodosNivel2 = 0;
	(*indiceV)->numeroEstrellasIndice = 0;

	free(*indiceV);
	*indiceV = NULL;
}

/* Funcion que libera la memoria del indice Vector */


/* Funcion de recorrido del indice */

void RecorrerIndiceVector_2(IndiceVector2 indiceV, int *contador, ListaDatosCrowding listCrowd, ListaDatosCrowding list) {
	*contador = 0;
	int i, j, k;
	for (i = 0; i < indiceV->numeroNodosNivel1; i++) {

		for (j = indiceV->indiceNivel1[i].lower; j <= indiceV->indiceNivel1[i].upper; j++) {

			for (k = indiceV->indiceNivel2[j].lower; k <= indiceV->indiceNivel2[j].upper; k++) {

				list[(*contador) * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_M1I] = listCrowd[indiceV->vector_ids[k] * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_M1I];
				list[(*contador) * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_M2I] = listCrowd[indiceV->vector_ids[k] * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_M2I];
				list[(*contador) * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_M1O] = listCrowd[indiceV->vector_ids[k] * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_M1O];
				list[(*contador) * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_M2O] = listCrowd[indiceV->vector_ids[k] * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_M2O];
				list[(*contador) * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_X] = listCrowd[indiceV->vector_ids[k] * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_X];
				list[(*contador) * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_Y] = listCrowd[indiceV->vector_ids[k] * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_Y];
				(*contador)++;
			}
		}
	}
}

/* Funcion de recorrido del indice */


/* Funcion de informacion del indice */

void InformacionIndiceVector_2(IndiceVector2 indiceV, int recorrer) {
	printf("Informacion general del indice:\n\n");
	printf("Numero de datos en el nivel 1: %d\n", indiceV->numeroNodosNivel1);
	printf("Numero de datos en el nivel 2: %d\n", indiceV->numeroNodosNivel2);
	printf("Numero de estrellas totales en el indice: %d\n", indiceV->numeroEstrellasIndice);

	printf("\n");

	if (recorrer) {

		printf("Informacion del indice en forma de estructura:\n\n");
		printf("Exp1\t\tExp2\t\tIDs\n");
		int i, j, k;
		for (i = 0; i < indiceV->numeroNodosNivel1; i++) {

			printf("%lf\n", indiceV->indiceNivel1[i].dato);

			for (j = indiceV->indiceNivel1[i].lower; j <= indiceV->indiceNivel1[i].upper; j++) {

				printf("\t\t%lf\n", indiceV->indiceNivel2[j].dato);

				printf("\t\t\t\t");

				for (k = indiceV->indiceNivel2[j].lower; k <= indiceV->indiceNivel2[j].upper; k++) {
					printf("%d", indiceV->vector_ids[k]);
					if (k < indiceV->indiceNivel2[j].upper) {
						printf(", ");
					}
				}

				printf("\n");

			}
		}

		printf("\n");

		printf("Informacion por vectores del indice:\n\n");

		printf("Vector Nivel M1i:\n");
		for (i = 0; i < indiceV->numeroNodosNivel1; i++) {
			printf("%lf [%d, %d]", indiceV->indiceNivel1[i].dato, indiceV->indiceNivel1[i].lower, indiceV->indiceNivel1[i].upper);
			if (i < (indiceV->numeroNodosNivel1 - 1)) {
				printf(" - ");
			}
		}

		printf("\n\n");

		printf("Vector Nivel Color:\n");
		for (i = 0; i < indiceV->numeroNodosNivel2; i++) {
			printf("%lf [%d, %d]", indiceV->indiceNivel2[i].dato, indiceV->indiceNivel2[i].lower, indiceV->indiceNivel2[i].upper);
			if (i < (indiceV->numeroNodosNivel2 - 1)) {
				printf(" - ");
			}
		}

		printf("\n\n");

		printf("Vector IDs:\n");
		for (i = 0; i < indiceV->numeroEstrellasIndice; i++) {
			printf("%d ", indiceV->vector_ids[i]);
		}

		printf("\n\n");
	}
}

/* Funcion de informacion del indice */


/* Funcion de busqueda */

void BuscarEstrellasIndiceVector_2(IndiceVector2 indiceV, tipoDato *rangos, int *contador, ListaID list, int estrellaDispersoEntero, ListaDatosDispersoEntero *listaDatosDispersoEntero) {

	*contador = 0;

	int lower_index_1 = busquedaBinariaIndiceVectorInferior(indiceV->indiceNivel1, 0, indiceV->numeroNodosNivel1 - 1, rangos[ERROR_DATO_1_MENOS]);
	int upper_index_1 = busquedaBinariaIndiceVectorSuperior(indiceV->indiceNivel1, 0, indiceV->numeroNodosNivel1 - 1, rangos[ERROR_DATO_1_MAS]);

	if (lower_index_1 >= 0 && upper_index_1 >= 0) {

		int i, j, k;

		for (i = lower_index_1; i <= upper_index_1; i++) {

			int lower_index_2 = busquedaBinariaIndiceVectorInferior(indiceV->indiceNivel2, indiceV->indiceNivel1[i].lower, indiceV->indiceNivel1[i].upper, rangos[ERROR_DATO_2_MENOS]);
			int upper_index_2 = busquedaBinariaIndiceVectorSuperior(indiceV->indiceNivel2, indiceV->indiceNivel1[i].lower, indiceV->indiceNivel1[i].upper, rangos[ERROR_DATO_2_MAS]);

			if (lower_index_2 >= 0 && upper_index_2 >= 0) {

				for (j = lower_index_2; j <= upper_index_2; j++) {

					for (k = indiceV->indiceNivel2[j].lower; k <= indiceV->indiceNivel2[j].upper; k++) {

						if ((*contador) < MAX_ESTRELLAS_SELECCIONADAS) {
							list[(*contador)] = indiceV->vector_ids[k];
							(*contador)++;
						}
						else {
							(*listaDatosDispersoEntero[estrellaDispersoEntero + COL_DISPERSO_OVER_BUFFER]) = 1;
							return;
						}

					}

				}
			}
		}
	}
}

/* Funcion de busqueda */

