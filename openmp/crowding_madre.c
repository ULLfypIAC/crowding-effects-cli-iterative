#include "crowding_madre.h"

// Funcion que lee el fichero de crowding
ListaDatosCrowding LeerFicheroCrowding_n(ParametrosEjecucion *parametrosEjecucion) {

	char linea[MAX_TAM_BUFFER];
	char datos[NUMERO_COLUMNAS_CROWDING][MAX_TAM_BUFFER];

	FILE *fcrowd;
	fcrowd = fopen(parametrosEjecucion->fichCrowdingEntrada, "r");

	if (fcrowd == NULL) {
		printf("Error: El fichero de crowding %s no existe en el sistema de ficheros...\n", parametrosEjecucion->fichCrowdingEntrada);
		exit(EXIT_FAILURE);
	}

	// Contamos el numero de estrellas que hay en el fichero Crowding
	int i = 0;
	int k = 1;
	int badLine;
	while (!feof(fcrowd)) {

		memset(linea, '\0', MAX_TAM_BUFFER);

		fgets(linea, MAX_TAM_BUFFER, fcrowd);

		// Comprobando que la linea no tiene caracteres ilegales
		int j;
		badLine = 0;
		for (j = 0; (j < MAX_TAM_BUFFER && linea[j] != '\0' && linea[j] != 10 && linea[j] != 13 && badLine == 0); j++) {

			if (!(linea[j] >= '0' && linea[j] <= '9') && linea[j] != '.' && linea[j] != 'e' && linea[j] != 'E' && linea[j] != '+' && linea[j] != '-' && linea[j] != ' ') {
				badLine = 1;
				printf("Error en la linea %d del fichero crowding. Linea ignorada...\n", k);
			}
		}

		if (badLine == 0) {			// La linea no contiene caracteres ilegales

			if (j == 0) {									// La linea no tiene caracteres
				k++;
				continue;
			}

			int result = sscanf(linea, "%s %s %s %s %s %s", datos[COL_CROWDING_M1I], datos[COL_CROWDING_M2I], datos[COL_CROWDING_M1O], datos[COL_CROWDING_M2O], datos[COL_CROWDING_X], datos[COL_CROWDING_Y]); // La linea tiene caracteres

			if (result != -1)				// Se ha encontrado por lo menos una columna

				if (result < NUMERO_COLUMNAS_CROWDING)		// El numero de columnas buscadas es erroneo
					printf("Error en la linea %d del fichero crowding. Linea ignorada...\n", k);
				else
					i++;					// El numero de columnas buscadas es correcto

			k++;						// No se ha encontrado ninguna columna

			continue;
		}

		k++;					// La linea contiene caracteres ilegales
	}

	if (i == 0) {
		printf("Error: El fichero de crowding %s esta vacio...\n", parametrosEjecucion->fichCrowdingEntrada);
		exit(EXIT_FAILURE);
	}

	parametrosEjecucion->totalCrowding = i;

	// Creando la lista de estrellas
	ListaDatosCrowding listaDatosCrowding = (pTipoDato)malloc(parametrosEjecucion->totalCrowding * NUMERO_COLUMNAS_CROWDING * sizeof(tipoDato));

	// Leemos las estrellas del fichero crowding y las guardamos en una lista, para ello debemos situar el puntero del fichero al principio
	rewind(fcrowd);

	i = 0;
	while (!feof(fcrowd)) {

		memset(linea, '\0', MAX_TAM_BUFFER);

		fgets(linea, MAX_TAM_BUFFER, fcrowd);

		int j;
		badLine = 0;
		for (j = 0; (j < MAX_TAM_BUFFER && linea[j] != '\0' && linea[j] != 10 && linea[j] != 13 && badLine == 0); j++) {

			if (!(linea[j] >= '0' && linea[j] <= '9') && linea[j] != '.' && linea[j] != 'e' && linea[j] != 'E' && linea[j] != '+' && linea[j] != '-' && linea[j] != ' ')
				badLine = 1;

		}

		if (badLine == 0) {
			int result = sscanf(linea, "%s %s %s %s %s %s", datos[COL_CROWDING_M1I], datos[COL_CROWDING_M2I], datos[COL_CROWDING_M1O], datos[COL_CROWDING_M2O], datos[COL_CROWDING_X], datos[COL_CROWDING_Y]);
			if ((result != -1) && (result == NUMERO_COLUMNAS_CROWDING)) {
				listaDatosCrowding[i * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_M1I] = atof(datos[COL_CROWDING_M1I]);
				listaDatosCrowding[i * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_M2I] = atof(datos[COL_CROWDING_M2I]);
				listaDatosCrowding[i * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_M1O] = atof(datos[COL_CROWDING_M1O]);
				listaDatosCrowding[i * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_M2O] = atof(datos[COL_CROWDING_M2O]);
				listaDatosCrowding[i * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_X] = atof(datos[COL_CROWDING_X]);
				listaDatosCrowding[i * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_Y] = atof(datos[COL_CROWDING_Y]);
				i++;
			}
		}
	}

	fclose(fcrowd);

	return listaDatosCrowding;
}


// Funcion que lee el fichero madre
ListaDatosMadre LeerFicheroMadre_n(ParametrosEjecucion *parametrosEjecucion) {

	char linea[MAX_TAM_BUFFER];
	char datos[NUMERO_COLUMNAS_MADRE][MAX_TAM_BUFFER];

	FILE *fmadre;
	fmadre = fopen(parametrosEjecucion->fichMadre, "r");

	if (fmadre == NULL) {
		printf("Error: El fichero madre %s no existe en el sistema de ficheros...\n", parametrosEjecucion->fichMadre);
		exit(EXIT_FAILURE);
	}

	// Contamos el numero de estrellas que hay en el fichero Madre
	int i = 0;
	int k = 1;
	int badLine;
	while (!feof(fmadre)) {

		memset(linea, '\0', MAX_TAM_BUFFER);

		fgets(linea, MAX_TAM_BUFFER, fmadre);

		int j;
		badLine = 0;
		for (j = 0; (j < MAX_TAM_BUFFER && linea[j] != '\0' && linea[j] != 10 && linea[j] != 13 && badLine == 0); j++) {

			if (!(linea[j] >= '0' && linea[j] <= '9') && linea[j] != '.' && linea[j] != 'e' && linea[j] != 'E' && linea[j] != '+' && linea[j] != '-' && linea[j] != ' ') {
				badLine = 1;
				printf("Error en la linea %d del fichero madre. Linea ignorada...\n", k);
			}
		}

		if (badLine == 0) {			// La linea no contiene caracteres ilegales

			if (j == 0) {									// La linea no tiene caracteres
				k++;
				continue;
			}

			int result = sscanf(linea, "%s %s %s %s %s %s", datos[COL_MADRE_M1], datos[COL_MADRE_M2], datos[COL_MADRE_EDAD], datos[COL_MADRE_METALICIDAD], datos[COL_MADRE_X], datos[COL_MADRE_Y]); // La linea tiene caracteres

			if (result != -1)				// Se ha encontrado por lo menos una columna

				if (result < NUMERO_COLUMNAS_MADRE)		// El numero de columnas buscadas es erroneo
					printf("Error en la linea %d del fichero madre. Linea ignorada...\n", k);
				else
					i++;					// El numero de columnas buscadas es correcto

			k++;						// No se ha encontrado ninguna columna

			continue;
		}

		k++;					// La linea contiene caracteres ilegales
	}

	if (i == 0) {
		printf("Error: El fichero madre %s esta vacio...\n", parametrosEjecucion->fichMadre);
		exit(EXIT_FAILURE);
	}

	parametrosEjecucion->totalMadre = i;

	// Creando la lista de estrellas
	ListaDatosMadre listaDatosMadre = (pTipoDato)malloc(parametrosEjecucion->totalMadre * NUMERO_COLUMNAS_MADRE * sizeof(tipoDato));

	// Leemos las estrellas del fichero madre y las guardamos en una lista, para ello debemos situar el puntero del fichero al principio
	rewind(fmadre);

	i = 0;
	while (!feof(fmadre)) {

		memset(linea, '\0', MAX_TAM_BUFFER);

		fgets(linea, MAX_TAM_BUFFER, fmadre);

		int j;
		badLine = 0;
		for (j = 0; (j < MAX_TAM_BUFFER && linea[j] != '\0' && linea[j] != 10 && linea[j] != 13 && badLine == 0); j++) {

			if (!(linea[j] >= '0' && linea[j] <= '9') && linea[j] != '.' && linea[j] != 'e' && linea[j] != 'E' && linea[j] != '+' && linea[j] != '-' && linea[j] != ' ')
				badLine = 1;
		}

		if (badLine == 0) {
			int result = sscanf(linea, "%s %s %s %s %s %s", datos[COL_MADRE_M1], datos[COL_MADRE_M2], datos[COL_MADRE_EDAD], datos[COL_MADRE_METALICIDAD], datos[COL_MADRE_X], datos[COL_MADRE_Y]);
			if ((result != -1) && (result == NUMERO_COLUMNAS_MADRE)) {
				listaDatosMadre[i * NUMERO_COLUMNAS_MADRE + COL_MADRE_M1] = atof(datos[COL_MADRE_M1]);
				listaDatosMadre[i * NUMERO_COLUMNAS_MADRE + COL_MADRE_M2] = atof(datos[COL_MADRE_M2]);
				listaDatosMadre[i * NUMERO_COLUMNAS_MADRE + COL_MADRE_EDAD] = atof(datos[COL_MADRE_EDAD]);
				listaDatosMadre[i * NUMERO_COLUMNAS_MADRE + COL_MADRE_METALICIDAD] = atof(datos[COL_MADRE_METALICIDAD]);
				listaDatosMadre[i * NUMERO_COLUMNAS_MADRE + COL_MADRE_X] = atof(datos[COL_MADRE_X]);
				listaDatosMadre[i * NUMERO_COLUMNAS_MADRE + COL_MADRE_Y] = atof(datos[COL_MADRE_Y]);
				i++;
			}
		}
	}

	fclose(fmadre);

	return listaDatosMadre;
}

void EscribirListaDatosCrowdingFichero(ListaDatosCrowding list, int contador, char *nombre_fich) {
	FILE *fich;
	fich = fopen(nombre_fich, "w");
	int i;
	for (i = 0; i < contador; i++)
		fprintf(fich, "%lf %lf %lf %lf %lf %lf\n",	list[i * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_M1I],
								list[i * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_M2I],
								list[i * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_M1O],
								list[i * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_M2O],
								list[i * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_X],
								list[i * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_Y]);
	fclose(fich);
}

void EscribirListaDatosCrowdingConsola(ListaDatosCrowding list, int contador) {
	int i;
	printf("M1i\t\tM2i\t\tM1o\t\tM2o\t\tX\t\tY\n");
	for (i = 0; i < contador; i++)
		printf("%lf\t%lf\t%lf\t%lf\t%lf\t%lf\n",	list[i * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_M1I],
								list[i * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_M2I],
								list[i * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_M1O],
								list[i * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_M2O],
								list[i * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_X],
								list[i * NUMERO_COLUMNAS_CROWDING + COL_CROWDING_Y]);
}

void EscribirListaDatosMadreFichero(ListaDatosMadre list, int contador, char *nombre_fich) {
	FILE *fich;
	fich = fopen(nombre_fich, "w");
	int i;
	for (i = 0; i < contador; i++)
		fprintf(fich, "%lf %lf %lf %lf %lf %lf\n",	list[i * NUMERO_COLUMNAS_MADRE + COL_MADRE_M1],
								list[i * NUMERO_COLUMNAS_MADRE + COL_MADRE_M2],
								list[i * NUMERO_COLUMNAS_MADRE + COL_MADRE_EDAD],
								list[i * NUMERO_COLUMNAS_MADRE + COL_MADRE_METALICIDAD],
								list[i * NUMERO_COLUMNAS_MADRE + COL_MADRE_X],
								list[i * NUMERO_COLUMNAS_MADRE + COL_MADRE_Y]);
	fclose(fich);
}

void EscribirListaDatosMadreConsola(ListaDatosMadre list, int contador) {
	int i;
	printf("M1\t\tM2\t\tEdad\t\t\tMetalicidad\tX\t\tY\n");
	for (i = 0; i < contador; i++)
		printf("%lf\t%lf\t%lf\t%lf\t%lf\t%lf\n",	list[i * NUMERO_COLUMNAS_MADRE + COL_MADRE_M1],
								list[i * NUMERO_COLUMNAS_MADRE + COL_MADRE_M2],
								list[i * NUMERO_COLUMNAS_MADRE + COL_MADRE_EDAD],
								list[i * NUMERO_COLUMNAS_MADRE + COL_MADRE_METALICIDAD],
								list[i * NUMERO_COLUMNAS_MADRE + COL_MADRE_X],
								list[i * NUMERO_COLUMNAS_MADRE + COL_MADRE_Y]);
}

