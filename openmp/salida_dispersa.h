#ifndef SALIDA_DISPERSA_H
#define SALIDA_DISPERSA_H

#include <stdlib.h>
#include <stdio.h>

#include "tipos.h"

void imprimir_fichero_disperso(ListaDatosDispersoEntero listaDatosDispersoEntero, ListaDatosDispersoReal listaDatosDispersoReal, ParametrosEjecucion parametrosEjecucion);
void imprimir_fichero_selecciones(ListaDatosDispersoEntero listaDatosDispersoEntero, ParametrosEjecucion parametrosEjecucion);

#endif