#include <time.h>

#include "parametros_entrada.h"
#include "crowding_madre.h"
#include "calculo_expresiones.h"
#include "salida_dispersa.h"

#include "indice_avl2.h"
#include "indice_vector2.h"
#include "dispersion2.h"
#include "salida_crowding2.h"

#include "indice_avl4.h"
#include "indice_vector4.h"
#include "dispersion4.h"
#include "salida_crowding4.h"

int main(int argc, char **argv) {

	ParametrosEjecucion parametrosEjecucion = leerParametrosEntrada(argc, argv);

	erroresParametrosEntrada(parametrosEjecucion, argv[1]);

	printf("Fichero de parametros: %s\n", argv[1]);
	mostrarParametrosEntrada(parametrosEjecucion);


	float elapsedTime_read;
	clock_t start_read = clock();


	ListaDatosCrowding listaDatosCrowding = LeerFicheroCrowding_n(&parametrosEjecucion);

	ListaDatosMadre listaDatosMadre = LeerFicheroMadre_n(&parametrosEjecucion);

	ListaDatosDispersoReal listaDatosDispersoReal = (tipoDato *)malloc(parametrosEjecucion.totalMadre * NUM_COL_DISPERSO_FLOAT * sizeof(tipoDato));
	ListaDatosDispersoEntero listaDatosDispersoEntero = (int *)malloc(parametrosEjecucion.totalMadre * NUM_COL_DISPERSO_INT * sizeof(int));

	ListaUsoEstrellasCrowding listaUsoEstrellasCrowding = (int *)malloc(parametrosEjecucion.totalCrowding * sizeof(int));
	memset(listaUsoEstrellasCrowding, 0, parametrosEjecucion.totalCrowding * sizeof(int));


	clock_t stop_read = clock();
	elapsedTime_read = (float)(stop_read - start_read) / (float) CLOCKS_PER_SEC;
	printf("Tiempo para la lectura de los ficheros de entrada y reserva de memoria: %3.4f segundos\n", elapsedTime_read);



	float elapsedTime_expresions;
	clock_t start_expresions = clock();


	ListaDatosCrowdingExp listaDatosCrowdingExp = (pTipoDato)malloc(parametrosEjecucion.totalCrowding * parametrosEjecucion.numIndices * sizeof(tipoDato));
	calculoExprCrowding(listaDatosCrowding, parametrosEjecucion, listaDatosCrowdingExp);

	ListaDatosMadreExp listaDatosMadreExp = (pTipoDato)malloc(parametrosEjecucion.totalMadre * parametrosEjecucion.numIndices * sizeof(tipoDato));
	calculoExprMadre(listaDatosMadre, parametrosEjecucion, listaDatosMadreExp);

	ListaDatosCrowdingParcial listaDatosCrowdingParcial = (pTipoDato)malloc(parametrosEjecucion.totalCrowding * NUMERO_COLUMNAS_CROWDING_PARCIAL * sizeof(tipoDato));
	copiarDatosCrowdingParcial(listaDatosCrowding, parametrosEjecucion, listaDatosCrowdingParcial);


	clock_t stop_expresions = clock();
	elapsedTime_expresions = (float)(stop_expresions - start_expresions) / (float) CLOCKS_PER_SEC;
	printf("Tiempo para el calculo de expresiones: %3.4f segundos\n", elapsedTime_expresions);



	float elapsedTime_indiceVector2, elapsedTime_indiceVector4;

	// Creacion de Indice Vector
	IndiceAVL2 indiceA2;
	IndiceAVL4 indiceA4;
	IndiceVector2 indiceV2;
	IndiceVector4 indiceV4;

	if (parametrosEjecucion.numIndices == 2) {		// Numero de indices 2

		clock_t start_indiceVector2 = clock();

		// Construimos el indice AVL a partir de la lista de estrellas crowding.
		indiceA2 = CrearIndiceAVL_2(listaDatosCrowdingExp, parametrosEjecucion);
		// Creando el indice Vector a partir del indice AVL
		indiceV2 = CrearIndiceVector_2(indiceA2);
		// Liberamos la memoria reservada para crear el indice AVL.
		PodarIndiceAVL_2(&indiceA2);

		clock_t stop_indiceVector2 = clock();
		elapsedTime_indiceVector2 = (float)(stop_indiceVector2 - start_indiceVector2) / (float) CLOCKS_PER_SEC;
		printf("Tiempo para crear indice AVL 2, crear indice Vector 2 y liberar indice AVL 2: %3.4f segundos\n", elapsedTime_indiceVector2);

	}
	else {							// Numero de indices 4

		clock_t start_indiceVector4 = clock();

		// Construimos el indice AVL a partir de la lista de estrellas crowding.
		indiceA4 = CrearIndiceAVL_4(listaDatosCrowdingExp, parametrosEjecucion);
		// Creando el indice Vector a partir del indice AVL
		indiceV4 = CrearIndiceVector_4(indiceA4);
		// Liberamos la memoria reservada para crear el indice AVL.
		PodarIndiceAVL_4(&indiceA4);

		clock_t stop_indiceVector4 = clock();
		elapsedTime_indiceVector4 = (float)(stop_indiceVector4 - start_indiceVector4) / (float) CLOCKS_PER_SEC;
		printf("Tiempo para crear indice AVL 4, crear indice Vector 4 y liberar indice AVL 4: %3.4f segundos\n", elapsedTime_indiceVector4);

	}


	double elapsedTime_alg_avl_openmp_2, elapsedTime_alg_avl_openmp_4;
	if (parametrosEjecucion.numIndices == 2) {								// Numero de indices 2

		double start_alg = omp_get_wtime();

		crowd_avl_2index_2cond_openmp_O(listaDatosMadre, listaDatosMadreExp, indiceV2, listaDatosCrowdingParcial, parametrosEjecucion, listaDatosDispersoReal, listaDatosDispersoEntero, listaUsoEstrellasCrowding);

		double stop_alg = omp_get_wtime();
		elapsedTime_alg_avl_openmp_2 = stop_alg - start_alg;
		printf("Tiempo para algoritmo: %3.4f segundos\n", elapsedTime_alg_avl_openmp_2);

	}
	else {													// Numero de indices 4

		double start_alg = omp_get_wtime();

		crowd_avl_4index_4cond_openmp_O(listaDatosMadre, listaDatosMadreExp, indiceV4, listaDatosCrowdingParcial, parametrosEjecucion, listaDatosDispersoReal, listaDatosDispersoEntero, listaUsoEstrellasCrowding);

		double stop_alg = omp_get_wtime();
		elapsedTime_alg_avl_openmp_4 = stop_alg - start_alg;
		printf("Tiempo para algoritmo: %3.4f segundos\n", elapsedTime_alg_avl_openmp_4);

	}



	float elapsedTime_writeO;
	clock_t start_writeO = clock();

	if (parametrosEjecucion.numIndices == 2) {
		imprimir_fichero_crowding_ordenado_con_anotaciones_indice_vector_2(listaDatosCrowding, indiceV2, listaUsoEstrellasCrowding, parametrosEjecucion);
	}
	else {
		imprimir_fichero_crowding_ordenado_con_anotaciones_indice_vector_4(listaDatosCrowding, indiceV4, listaUsoEstrellasCrowding, parametrosEjecucion);
	}

	imprimir_fichero_disperso(listaDatosDispersoEntero, listaDatosDispersoReal, parametrosEjecucion);
	imprimir_fichero_selecciones(listaDatosDispersoEntero, parametrosEjecucion);


	clock_t stop_writeO = clock();
	elapsedTime_writeO = (float)(stop_writeO - start_writeO) / (float) CLOCKS_PER_SEC;
	printf("Tiempo para escritura de ficheros de salida: %3.4f segundos\n", elapsedTime_writeO);



	float elapsedTime_freeMem;
	clock_t start_freeMem = clock();


	free(parametrosEjecucion.epsilons);
	free(listaDatosCrowding);
	free(listaDatosCrowdingExp);
	free(listaDatosCrowdingParcial);
	free(listaDatosMadre);
	free(listaDatosMadreExp);
	free(listaDatosDispersoReal);
	free(listaDatosDispersoEntero);
	free(listaUsoEstrellasCrowding);
	int i;
	for(i = 0; i < MAX_NUM_INDICES; i++) {
		free(parametrosEjecucion.exps_madre[i]);
		free(parametrosEjecucion.exps_crowding[i]);
	}
	free(parametrosEjecucion.exps_madre);
	free(parametrosEjecucion.exps_crowding);

	if (parametrosEjecucion.numIndices == 2) {
		PodarIndiceVector_2(&indiceV2);
	}
	else {
		PodarIndiceVector_4(&indiceV4);
	}


	clock_t stop_freeMem = clock();
	elapsedTime_freeMem = (float)(stop_freeMem - start_freeMem) / (float) CLOCKS_PER_SEC;
	printf("Tiempo para liberacion de memoria: %3.4f segundos\n", elapsedTime_freeMem);

	printf("Fin de la ejecucion\n");

	printf("\n");

	return 0;
}