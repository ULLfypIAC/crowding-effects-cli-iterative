#ifndef TIPOS4_H
#define TIPOS4_H

#include "tipos.h"

//+-------------------------------------------------------

typedef struct _indiceAVL4 {

	Arbol indice;

	int numeroNodosNivel1;
	int numeroNodosNivel2;
	int numeroNodosNivel3;
	int numeroNodosNivel4;
	int numeroEstrellasIndice;

} tipoIndiceAVL4;

typedef tipoIndiceAVL4* IndiceAVL4;
typedef tipoIndiceAVL4* pIndiceAVL4;

//+-------------------------------------------------------

//x-------------------------------------------------------

typedef struct _indiceVector4 {

	int numeroEstrellasIndice;
	ListaID vector_ids;

	int numeroNodosNivel1;
	ListaIndicePlano indiceNivel1;

	int numeroNodosNivel2;
	ListaIndicePlano indiceNivel2;

	int numeroNodosNivel3;
	ListaIndicePlano indiceNivel3;

	int numeroNodosNivel4;
	ListaIndicePlano indiceNivel4;

} tipoIndiceVector4;

typedef tipoIndiceVector4* IndiceVector4;
typedef tipoIndiceVector4* pIndiceVector4;

//x-------------------------------------------------------

#endif